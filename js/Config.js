define(function () {

    var canvas_width = 1280;
    var canvas_height = 640;

    return {

        assets: {
            // Define sprites to be loaded here...
            sprites: {
                // player ships
                ship_destroyer: "images/sprites/ships/destroyer.png",
                ship_sentinel: "images/sprites/ships/sentinel.png",

                // letters
                letters: "images/sprites/letters.png",

                // weapons
                //weapon_cannon: "images/sprites/weapons/cannon.png",

                // hud
                hud_weapons: "images/sprites/hud/weapons.png",

                // Mine -- but not part of the game.
                paddle: "images/sprites/paddle.png"

            },

            // Define audio to be loaded here.
            audio: {
                bell: "audio/fx/dying.wav",
                zip: "audio/fx/zip.wav",
                coin_picked_up: "audio/fx/coin_picked_up.wav"
            },

            music: {
                background: "audio/music/madeon-technicolor.mp3"
            }
        },

        canvas: {
            width: canvas_width,
            height: canvas_height,
            debug_dirty: false,
            debug_entity_maps: false,
            debug_mbr: false
        },

        letter: {
            pickupTimeout: 3000, // ms on how long to wait to allow a letter to be picked up after its dropped.
            maxDistSq: 10000
        },

        ship: {
            distance_clamp: 150
        },

        collision: {
            hashmap: {
                cellsize: 32
            }
        },

        board: {
            latest_words_timeout: 20 * 1000,
            winning_score: 150
        }
    };
});