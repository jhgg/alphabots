define(['lib/class'], function (Class) {
    return Class.extend({
        source: null,
        panner: null,
        init: function (context, buffer) {
            var source = context.createBufferSource();
            source.buffer = this.buffer;
            var panner = context.createPanner();
            panner.coneOuterGain = 0.1;
            panner.coneOuterAngle = 180;
            panner.coneInnerAngle = 0;

            panner.connect(context.destination);
            source.connect(panner);
            this.source = source;
        },

        play: function() {},

        __get_loop: function () {
            return this.source.loop;
        },

        __set_loop: function (val) {
            this.source.loop = val;
        },

        __get_coneOuterGain: function () {
            return this.panner.coneOuterGain;
        },
        __set_coneOuterGain: function (val) {
            this.panner.coneOuterGain = val;
        },

        __get_coneOuterAngle: function () {
            return this.panner.coneOuterAngle;
        },
        __set_coneOuterAngle: function (val) {
            this.panner.coneOuterAngle = val;
        },

        __get_coneInnerAngle: function () {
            return this.panner.coneInnerAngle;
        },
        __set_coneInnerAngle: function (val) {
            this.panner.coneInnerAngle = val;
        }

    })

})