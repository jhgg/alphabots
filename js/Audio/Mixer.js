define(function() {
    var AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext;
    var context = new AudioContext();

    return {
        context: context,
        getContext: function() { return context; }
    };

});