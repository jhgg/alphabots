define(['Audio/Generators/Base'], function (Base) {
    return Base.extend({
        init: function (context) {
            this._super(context);
            this.x = 0;
        },

        process_audio: function (e) {
            var data = e.outputBuffer.getChannelData(0);
            for (var i = 0; i < data.length; ++i) {
                data[i] = Math.sin(this.x++) * Math.random();
            }
            console.log("Made", data.length);
        }
    })
});