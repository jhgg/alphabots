define(['lib/class'], function(Class) {
    return Class.extend({
        sample_size: 1280,
        num_input_channels: 1,
        num_output_channels: 1,

        init: function(context) {
            this.context = context;
            this.destination = this.context.destination;
            this.node = context.createJavaScriptNode(this.sample_size, this.num_input_channels,
                this.num_output_channels);

            this.node.onaudioprocess = this.process_audio.bind(this);
        },

        process_audio: function(e) {
            throw "Cannot use this";
        },

        play: function() {
            this.node.connect(this.destination);
        },

        pause: function() {
            this.node.disconnect();
        }
    })
});