define(['Audio/Generators/Base', 'lib/class', 'Helpers/Math/Random/LCC'], function (Base, Class, RNG) {
    var rng = new RNG();
    var irand = rng.nextInt.bind(rng);

    var MAX_RAND_ROWS = 30,
        RANDOM_BITS = 24,
        SHIFT = 8 * 8 - 24;

    var PinkNoiseChannelGenerator = Class.extend({
        init: function (rows) {
            rows = Math.min(rows, MAX_RAND_ROWS);
            this.index = 0;
            this.index_mask = (1 << rows) - 1;
            this.running_sum = 0;
            this.scalar = 1.0 / ((rows + 1) * (1 << (RANDOM_BITS - 1)));
            this.rows = [];
            for (var i = 0; i < rows; i++) {
                this.rows.push(0);
            }

            console.log("Scalar", this.scalar);

        },
        generate_noise: function () {
            var nRandom,
                sum,
                numZeros,
                n;

            this.index = (this.index + 1) & this.index_mask;

            if (this.index != 0) {
                n = this.index;
                numZeros = 0;
                while ((n & 1) == 0) {
                    n = n >> 1;
                    numZeros++;
                }

                this.running_sum -= this.rows[numZeros];
                nRandom = (irand()) >> SHIFT;
                this.running_sum += nRandom;
                this.rows[numZeros] = nRandom;

            }
            nRandom = (irand()) >> SHIFT;
            sum = this.running_sum + nRandom;

            return this.scalar * sum;
        }
    });

    return Base.extend({
        sample_size: 4096,
        init: function (context, channels, noise_rows) {
            this.num_input_channels = this.num_output_channels = channels;
            if (channels == 1) {
                this.noise_generator = new PinkNoiseChannelGenerator(noise_rows[0]);
                this.process_audio = this.process_audio_one;
            } else {
                this.noise_generators = [];
                for (var i = 0; i < channels; i++) {
                    this.noise_generators.push(new PinkNoiseChannelGenerator(noise_rows[i]));
                }
                this.process_audio = this.process_audio_multi;
            }

            this._super(context);
        },

        process_audio_one: function (e) {
            var data = e.outputBuffer.getChannelData(0);
            for (var i = 0; i < data.length; ++i) {
                data[i] = this.noise_generator.generate_noise();
            }
        },
        process_audio_multi: function (e) {
            var i, j, data;
            for (i = 0; i < this.num_output_channels; i++) {
                data = e.outputBuffer.getChannelData(i);
                for (j = 0; j < data.length; ++j) {
                    data[j] = this.noise_generators[i].generate_noise();
                }
            }
        }
    })


});