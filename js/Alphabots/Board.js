define([
    // Dependencies
    'Base/EventEmitter', 'Helpers/TimerGroup',

    // Libs
    'lib/underscore',

    // Alphabots - config
    'Config',

    // Alphabots Stuff
    'Alphabots/Scrabble/InfiniteBag', 'Alphabots/Entities/Letter'],
function(EventEmitter, TimerGroup, _, Config, InfiniteBag, Letter) {
    return EventEmitter.extend({
        init: function() {
            this._super();
            this.running = false;

            // arrays
            this.timers = [];
            this.players = [];
            this.letters = [];
            this.bag = new InfiniteBag;

            // elements
            this.$hud_latest_words = $("#latest-words");
            this.$clock = $("#clock");

            // timers;
            this.timers.push(this.latestWordsTimer = new TimerGroup);
            this.timers.push(this.gameTimers = new TimerGroup);

            this.letterSpawnMillisLeft = 0;
        },

        clearTimers: function() {
            _.each(this.timers, function(t) {
                t.cancelAll();
            })
        },

        startGame: function(durationSeconds) {
            this.clearTimers();
            _.each(this.players, function(player) {
                player.score = 0;
                player.solvedWordsElements = [];
                player.resetPlayerHud();
            });
            this.running = true;
            this.$hud_latest_words.children().remove();
            this.startCountdown(durationSeconds);
            this.on('update', this._update);
            this.trigger('game:start');
        },

        startCountdown: function(durationSeconds) {
            var gameEndTime = +new Date / 1000 | 0;
            gameEndTime += durationSeconds;
            var self = this;
            var lastTimeRemaining = 0;

            this.gameTimers.loop(function() {
                var timeRemaining = gameEndTime - (+new Date / 1000 | 0);
                if (lastTimeRemaining == timeRemaining)
                    return;

                lastTimeRemaining = timeRemaining;

                var minutes = (timeRemaining / 60) | 0;
                var seconds = timeRemaining % 60 + '';
                if(seconds.length == 1)
                    seconds = '0' + seconds;

                if (timeRemaining <= 0)
                    self.endGame();

                if (timeRemaining >= 0)
                    self.$clock.text(minutes + ':' + seconds);

            }, 250, true);
        },

        endGame: function(wonWithScore) {
            this.running = false;
            this.clearTimers();
            this.$hud_latest_words.children().remove();

            // Save letters in temp array, so that we have a stable array to mutate
            // when we're deleting letters.
            var letters = [];
            $.each(this.letters, function(i, letter) {
                letters.push(letter);
            });
            $.each(letters, function(i, letter) {
                letter.destroy();
            });
            this.trigger('game:end', {
                wonWithScore: wonWithScore,
                winningPlayer: this.players[0]
            });
            this.off('update', this._update);
        },

        _update: function(e) {
            this.letterSpawnMillisLeft -= e.dt;
            if (this.letterSpawnMillisLeft < 0) {
                this.spawnLetter();
                this.letterSpawnMillisLeft += 250 + (this.letters.length * 150);
            }
        },

        addPlayer: function(player) {
            this.players.push(player);
            this._bindPlayerEvents(player);
        },

        _bindPlayerEvents: function(player) {
            var self = this;

            player.on('solve:success', function(e) {
                self._addLatestWordEntry(this, e.letters, e.score, e.exact);
                if (e.playerScore >= Config.board.winning_score) {
                    this.endGame(true);
                }
            })
        },

        _addLatestWordEntry: function(player, letters, score, exact) {
            var $el = $("<li></li>")
                .append(
                    $("<span></span>")
                        .addClass("player-name")
                        .text(player.username)
                )
                .append(
                    $("<span></span>")
                        .addClass("letter-container")
                        .append(function() {
                            var $letters = [];
                            for(var i = 0; i < letters.length; i++) {
                                $letters.push(letters[i].createElement());
                            }
                            return $letters;
                        })
                )
                .append(
                    $("<span></span>")
                        .addClass("points")
                        .addClass(exact ? 'exact' : '')
                        .text(score + " pts")
                )
                .appendTo(this.$hud_latest_words);

            player.solvedWordsElements.push($el);
            this.latestWordsTimer.delay(function() {
                $el.fadeOut(function() { $el.remove(); });
            }, Config.board.latest_words_timeout);

        },

        spawnLetter: function() {
            var item = this.bag.getItem();
            var letter = new Letter(item.letter, item.multiplier);
            letter.moveToRandomPointOnCanvas();
            //letter.addInitialVelocity();
            this._addLetter(letter);
        },

        _addLetter: function(letter) {
            var self = this;

            // once the letter is destroyed, clear it from our memory too.
            letter.on('destroy', function() {
                for(var i = 0, l = self.letters.length; i < l; i++) {
                    // find the letter.
                    if (this === self.letters[i]) {
                        // remove it.
                        self.letters.splice(i, 1);

                        // fire something if no letters remain.
                        if (self.letters.length == 0)
                            self._allLettersDestroyed();

                        return;
                    }
                }
            });
            this.letters.push(letter);
        },

        _allLettersDestroyed: function() { }


    });
})
