define(['Alphabots/Scrabble/Helpers', 'lib/class', 'lib/underscore'], function(SHelpers, Class, _) {
    return Class.extend({
        init: function() {
            this._bag = [];
            this._fillBag();
        },

        _fillBag: function() {
            var self = this;
            SHelpers.iterBagItems(function(l) {
                self._bag.push({
                    letter: l.toUpperCase(),
                    multiplier: 0
                });
            });

            self._bag = _.shuffle(self._bag);

            for(var i = 0; i < 10; i++) {
                self._bag[i].multiplier = 1;
            }

            for(i = 10; i < 15; i++) {
                self._bag[i].multiplier = 2;
            }

            self._bag = _.shuffle(self._bag);
        },

        getItem: function() {
            if (!this._bag.length)
                this._fillBag();

            return this._bag.pop();
        },

        resetBag: function() {
            this._bag.length = 0;
            this._fillBag();
        }

    })

});