define(['Base/EventEmitter', 'jquery', 'Alphabots/Scrabble/Helpers', 'lib/underscore', 'Assets'],
    function (EventEmitter, $, Scrabble, _, Assets) {
        return EventEmitter.extend({
            // variables
            ship: null,
            controller: null,

            // functions
            init: function (board) {
                this.board = board;
                this.letters = [];
                this.score = 0;
                this.solvedWords = [];

                var is_self = true;

                if (is_self) {
                    this.$hud_stats = $("#player-self");
                    this.$hud_letters = this.$hud_stats.find('.letterbox');
                    this.$hud_username = this.$hud_stats.find('.username');
                    this.$hud_score = this.$hud_stats.find('.score');
                }

                this.resetPlayerHud();
                this.initPlayerHud();
                this.initSounds();
            },

            addShip: function (ship) {
                this.ship = ship;
            },

            addShipController: function (controllerClass) {
                if (!this.ship)
                    throw "Tried to add controller to a player with out a ship";

                if (this.controller)
                    throw "Player already has a controller";

                this.controller = new controllerClass(this, this.ship);
            },

            addLetter: function (letter) {
                if (letter.player === this)
                    return;

                while (this.letters.length >= 8)
                    this.letters[0].removeFromPlayer();

                this.letters.push(letter);
                letter.player = this;
                this.trigger('letter:add', letter);
            },

            removeLetter: function (letter) {
                for (var i = 0, l = this.letters.length; i < l; i++) {
                    var _letter = this.letters[i];
                    if (_letter === letter) {
                        this.letters.splice(i, 1);
                        letter.player = null;
                        this.trigger('letter:remove', letter);
                        return;
                    }
                }
            },

            resetPlayerHud: function () {
                this.$hud_letters.children().remove();
                this.$hud_score.text(0);
            },

            __set_username: function(v) {
                this._username = v;
                this.$hud_username.text(v);
            },
            __get_username: function() {
                return this._username;
            },

            getLetterString: function () {
                var letters = [];
                for (var i = 0, l = this.letters.length; i < l; i++)
                    letters.push(this.letters[i].getCharacter());

                return letters.join('').toLowerCase();
            },

            trySolveLetters: function () {
                var letters = this.getLetterString();
                if (letters.length < 2) {
                    this.trigger('solve:too-short', letters.length);
                    return;
                }

                var choices = Scrabble.unscrambleWord(letters);
                if (!choices) {
                    this.trigger('solve:fail', letters);
                    return;
                }

                // word is found, process!
                var foundIndex = _.indexOf(choices, letters);
                var foundWord = foundIndex == -1 ? choices[0] : choices[foundIndex];
                var scoreMultiplier = this._calcLettersMultiplier(foundIndex == -1 ? 1 : 2);
                var score = Scrabble.calculateWordValue(foundWord) * scoreMultiplier;

                // add the score.
                this.score += score;
                this.solvedWords.push({
                    word: foundWord,
                    score: score
                });

                // order the letters properly to pass to the next function.
                var ordered_letters;
                if (foundIndex != -1) {
                    ordered_letters = this.letters;
                } else {
                    var l_map = {};
                    _.each(this.letters, function (v) {
                        if (!l_map[v.letter])
                            l_map[v.letter] = [];
                        l_map[v.letter].push(v);
                    });
                    ordered_letters = [];
                    _.each(foundWord.toUpperCase(), function (l) {
                        ordered_letters.push(
                            l_map[l].shift()
                        );
                    });
                }

                // pass to all relevant subscribers
                this.trigger('solve:success', {
                    word: foundWord,
                    score: score,
                    playerScore: this.score,
                    letters: ordered_letters,
                    exact: foundIndex != -1,
                    multiplier: scoreMultiplier});
                this.destroyAllLetters();
            },

            _calcLettersMultiplier: function (mul) {
                var mul = mul | 1;
                for (var i = 0, l = this.letters.length; i < l; i++)
                    mul *= this.letters[i].getMultiplier();

                return mul;
            },

            dropAllLetters: function () {
                while (this.letters.length)
                    this.letters[0].removeFromPlayer(true);
            },

            dropFirstLetter: function () {
                if (this.letters.length)
                    this.letters[0].removeFromPlayer(true);
            },

            dropLastLetter: function () {
                if (this.letters.length)
                    this.letters[this.letters.length - 1].removeFromPlayer(true);
            },

            destroyAllLetters: function () {
                while (this.letters.length)
                    this.letters[0].destroy();
            },

            initPlayerHud: function () {
                this.on('letter:add', function (letter) {
                    var $letter = letter.createElement(letter);
                    letter.$letter = $letter;
                    $letter.appendTo(this.$hud_letters);
                });

                this.on('letter:remove', function (letter) {
                    if (!letter.$letter)
                        return;

                    letter.$letter.remove();
                    letter.$letter = null;
                });

                this.on('solve:success', function (e) {
                    this.$hud_score.text(this.score);
                });
            },

            initSounds: function () {
                this.on('letter:add', function (letter) {
                    Assets.play_sound_by_name("coin_picked_up");
                })
            }

        })

    });