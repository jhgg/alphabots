define(['Helpers/Input/Mouse', 'lib/class', 'Helpers/Math/Position', 'Config'],
    function (Mouse, Class, Position, Config) {
        "use strict";

        var DEG_TO_RAD = 180 / Math.PI,
            RAD_TO_DEG = Math.PI / 180;

        return Class.extend({
            init: function (player, entity) {
                this.player = player;
                this.entity = entity;
                this.velocityFactor = 900;
                this.maxRotationPerFrame = Math.PI / 40;

                this.target_rotation = entity.rotation;
                var self = this;
                entity.centerOrigin(true);
                entity.on('update', function (e) {
                    self.update(e);
                });

            },

            update: function updatePlayerShip(e) {
                if (!this.player.board.running)
                    return;

                var entity = this.entity;
                var pos = Mouse.position;
                var absolutePosition = Position.getAbsolutePositionOfCanvasSprite(entity);

                this.target_rotation = Position.getAngle(pos, absolutePosition) / (Math.PI / 180);
                var dist = Position.getExactDist2d(pos, absolutePosition);

                dist = Math.min(dist, Config.ship.distance_clamp);
                var factor = dist / this.velocityFactor;
                if (dist < 8) {
                    factor = 0;
                }

                if (entity._rotation != this.target_rotation && factor) {
                    var A = entity._rotation / DEG_TO_RAD;
                    var tA = this.target_rotation / DEG_TO_RAD;

                    var dA = Math.min(this.maxRotationPerFrame, Math.abs(A - tA));
                    if (Position.normalizeOrientation(A - tA) < Math.PI) {
                        dA *= -1;
                    } else {
                    }

                    A = Position.normalizeOrientation(A + dA);
                    entity.rotation = A / RAD_TO_DEG;
                }

                var rotDeg = entity._rotation / DEG_TO_RAD;

                entity.x += Math.cos(rotDeg) * factor * e.dt;
                entity.y += Math.sin(rotDeg) * factor * e.dt;
            }
        });
    });

