define(['Canvas/CollidingEntity', 'Assets', 'Helpers/Math', 'Config', 'Helpers/Math/Position', 'Collision/Masks', 'jquery'],
function (CollidingEntity, Assets, HMath, Config, Position, Masks, $) {

    var DEG_TO_RAD = 180 / Math.PI;
    return CollidingEntity.extend({
        sprite: Assets.sprites.letters,
        c: 'letter',
        player: null,
        init: function (letter, multiplier) {
            if (!/^[A-Z]$/.test(letter))
                throw "Not a letter!";

            this._super();
            this.w = 25;
            this.h = 25;
            this.letter = letter;
            this.nextPickupTime = 500;
            this.collision_mask = Masks.SHIP;
            this.updateAlpha();

            this.z = 1000;
            this.multiplier = multiplier || 0;
            var letterPosition = letter.toUpperCase().charCodeAt(0) - 65;
            this.offsetY = 25 * letterPosition;
            this.offsetX = 25 * this.multiplier;
            this.centerOrigin(true);

            // Remove default draw handler, we'll use our own.
            this.off('draw');

            this.init_collision_with_poly();
            this.friction = 0.95;
            this.velocity_x = 0;
            this.velocity_y = 0;
            this.on('update', function (e) {
                this.velocity_x *= this.friction;
                this.velocity_y *= this.friction;

                this.x += this.velocity_x;
                this.y += this.velocity_y;

                if (this.nextPickupTime > 0) {
                    this.nextPickupTime -= e.dt;
                    this.updateAlpha();
                }

            });

            this.on('draw', function (e) {

                if (this.x > $$game.canvasWidth + this._w) {
                    this.x = 0;
                } else if (this.x < -this._w) {
                    this.x = $$game.canvasWidth;
                }
                if (this.y > $$game.canvasHeight + this._h) {
                    this.y = 0;
                } else if (this.y < -this._y) {
                    this.y = $$game.canvasHeight;
                }

                e.ctx.drawImage(this.sprite, this.offsetX, this.offsetY,
                    this._w, this._h, e.pos._x, e.pos._y, this._w, this._h);
            });

            this.on('destroy', this._onDestroy);
        },

        updateAlpha: function () {
            this.alpha = (Math.max(-500, -this.nextPickupTime) + 1000) / 1000;
            this.collision_mask = this.canBePickedUp() ? Masks.SHIP : 0;
        },

        createElement: function (letter) {
            return $("<span></span>")
                .addClass('letter')
                .addClass('mul-' + (this.multiplier + 1) + 'x')
                .addClass('new')
                .text(this.letter);
        },

        canBePickedUp: function () {
            return (!this.player) && (this.nextPickupTime <= 0);
        },

        getCharacter: function () {
            return this.letter;
        },

        getMultiplier: function () {
            return this.multiplier + 1
        },

        addToPlayer: function (player) {
            if (this.player && this.player === player)
                return;

            if (this.player)
                this.removeFromPlayer();

            player.addLetter(this);
            this.updateAlpha();
            this.followShip();
        },


        followShip: function () {
            this.on('update', this._followShipUpdate);
        },

        _followShipUpdate: function () {
            var i = (Math.random() + ((3 - this.multiplier) / 5)) / 3;
            var dist = HMath.distanceSq(this.player.ship._x, this.player.ship._y, this._x, this._y);
            if (dist > Config.letter.maxDistSq) {
                this.removeFromPlayer();
                return;
            }

            this.velocity_x += HMath.clamp(this.player.ship._x - this._x, -i, i);
            this.velocity_y += HMath.clamp(this.player.ship._y - this._y, -i, i);
        },

        _onDestroy: function () {
            if (this.player)
                this.player.removeLetter(this);
        },

        removeFromPlayer: function (eject) {
            if (!this.player)
                return;

            var ship = this.player.ship;

            this.nextPickupTime = Config.letter.pickupTimeout;
            this.off('update', this._followShipUpdate);
            this.player.removeLetter(this);
            this.updateAlpha();

            if (eject) {
                var offset = 180 - 30 + Math.random() * 60;
                var ejectAngle = (ship.rotation - offset) / DEG_TO_RAD;
                var radius = ((this.multiplier) * 4) + 5 + Math.random() * 5;

                this.velocity_x = radius * Math.cos(ejectAngle);
                this.velocity_y = radius * Math.sin(ejectAngle);
            }
        }

    });
});