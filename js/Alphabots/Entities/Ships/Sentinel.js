define(['Alphabots/Entities/Ships/Base', 'Assets', 'Collision/Shape/Polygon'], function (Base, Assets, Polygon) {
    return Base.extend({
        sprite: Assets.sprites.ship_sentinel,
        poly: new Polygon([
            [12, 3],
            [16, 3],
            [26, 16],
            [41, 19],
            [47, 22],
            [43, 23],
            [44, 25],
            [43, 28],
            [46, 28],
            [41, 31],
            [33, 32],
            [26, 35],
            [16, 48],
            [12, 46],
            [12, 38],
            [8, 32],
            [4, 32],
            [7, 28],
            [4, 25],
            [7, 22],
            [3, 19],
            [10, 18],
            [11, 14]
        ])
    });
});