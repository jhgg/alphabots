define(['Alphabots/Entities/Ships/Base', 'Assets', 'Collision/Shape/Polygon'], function (Base, Assets, Polygon) {
    return Base.extend({
        sprite: Assets.sprites.ship_destroyer,
        poly: new Polygon([
            [9, 6],
            [69, 35],
            [9, 65],
            [8, 50],
            [5, 41],
            [5, 30],
            [9, 20]
        ])
    });
});