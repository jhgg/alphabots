define(['Canvas/CollidingEntity'], function(CollidingEntity) {
    return CollidingEntity.extend({
        init: function() {
            this._super();
            this.init_collision_with_poly(this.poly && this.poly.clone())
        }
    });
});