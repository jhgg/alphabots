define(['Canvas/CollidingEntity', 'Alphabots/Player/ShipController', 'Collision/Shape/Polygon',
        'Alphabots/Entities/Ships/Destroyer', 'Alphabots/Entities/Ships/Sentinel', 'Animation/Particles',
        "Alphabots/Entities/Letter", 'Alphabots/Board', 'Alphabots/Player', 'Alphabots/Scrabble/InfiniteBag',
        'lib/Keypress'],
    function (CanvasEntity, SC, Poly, Destroyer, Sentinel, Particles, Letter, Board, Player, InfiniteBag, keypress) {

        return function () {
            var board = new Board;
            var pl = new Player(board);
            board.addPlayer(pl);
            board.startGame(120);
            var e = new Sentinel;
            pl.username = "Player Juan";
            pl.addShip(e);
            pl.addShipController(SC);
            e.collision_mask = 0x01 | 0x02 | 0x04;


            e.y = 400;
            e.x = 400;

            e.onHit(e.collision_mask, function (data) {
                _.each(data, function (c) {
                    if (c.obj.c === 'letter')
                        c.obj.addToPlayer(pl);

                });
            });

            keypress.combo('s', function () {
                pl.trySolveLetters();
            });

            keypress.combo('d', function () {
                pl.dropAllLetters();
            });

            keypress.combo('w', function () {
                pl.dropFirstLetter();
            });

            keypress.combo('e', function () {
                pl.dropLastLetter();
            });

            var $game_over = $("#game-over");
            var $word_list = $("#word-list");
            var $start_again_btn = $game_over.find('[data-play-again]');

            board.on('game:end', function(e) {
                $start_again_btn.one('click', function() {
                    if (!board.running)
                        board.startGame();
                });
                $word_list.children().remove();
                console.log(e);
                $word_list.append(e.winningPlayer.solvedWordsElements);
                $word_list.find('.player-name').remove();
                $game_over.show();
            });

            board.on('game:start', function() {
                $game_over.hide();
            });

        }
    })
;
