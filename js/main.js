require.config({
    urlArgs: "bust=" + (new Date()).getTime(),

    paths: {
        'Class': 'lib/class',
        '_': 'lib/underscore',
        'text': 'lib/text',
        'keypress': 'lib/keypress',
        'jquery': 'lib/jquery'
    }

});

require(['Game', 'Debugger/Client'], function(Game, DC) {
    Game.init();
    DC.bindDebugStartButton('#debug');
});