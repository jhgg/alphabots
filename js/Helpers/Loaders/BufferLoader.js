define(['lib/underscore'], function (_) {
    "use strict";

    function BufferLoader(context, urlMap, callback, progress) {
        this.context = context;
        this.urlMap = urlMap;
        this.size = _.size(urlMap);
        this.onload = callback;
        this.onprogress = progress;
        this.bufferMap = {};
        this.loadCount = 0;
    }

    BufferLoader.prototype.loadBuffer = function (url, key) {
        // Load buffer asynchronously
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.responseType = "arraybuffer";

        var loader = this;

        request.onload = function () {
            // Asynchronously decode the audio file data in request.response
            loader.context.decodeAudioData(
                request.response,
                function (buffer) {
                    if (!buffer) {
                        alert('error decoding file data: ' + url);
                        return;
                    }
                    loader.bufferMap[key] = buffer;
                    if (++loader.loadCount == loader.size)
                        loader.onload(loader.bufferMap);

                    else if (loader.onprogress)
                        loader.onprogress(loader.loadCount, loader.size);

                },
                function (error) {
                    console.error('decodeAudioData error', error);
                }
            );
        };

        request.onerror = function () {
            alert('BufferLoader: XHR error');
        };

        request.send();
    };

    BufferLoader.prototype.load = function () {
        _.each(this.urlMap, this.loadBuffer.bind(this));
        return this;
    };

    return {
        loadBufferMap: function (context, urlmap, callback, progress) {
            return (new BufferLoader(context, urlmap, callback, progress)).load();
        }
    }
});