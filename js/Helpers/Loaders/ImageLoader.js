define(['lib/underscore'], function (_) {
    "use strict";

    // private loadImage function.
    function loadImage(image_src, load_callback) {
        var image = new Image();
        image.src = image_src;
        image.addEventListener("load", function(e) {
            load_callback(image);
        }, false);
        return image;
    }

    return {
        // Loads all the images referred to by the image URLs in the "images" array.
        loadImages: function (images, all_loaded_callback, progress_callback) {
            var domImages = [];
            var i = 0, total = images.length;
            _.each(images, function (image) {
                loadImage(image, function (domImage) {
                    domImages.push(domImage);

                    if (++i === total)
                        all_loaded_callback(domImages);

                    else if (progress_callback)
                        progress_callback(image, i, total)

                });
            });
        },

        // Loads all images referred to by the image URLs in the "images" object.
        loadImagesMap: function (images, all_loaded_callback, progress_callback) {
            var domImages = {};
            var i = 0, total = _.size(images);
            _.each(images, function (image, key) {
                loadImage(image, function (domImage) {
                    domImages[key] = domImage;
                    if (++i === total)
                        all_loaded_callback(domImages);

                    else if (progress_callback)
                        progress_callback(image, i, total);

                })
            })
        }
    }
});