define({
    clamp: function (val, min, max) {
        if (val > max)
            return max;
        if (val < min)
            return min;
        return val;

    },

    degToRad: function (deg) {
        return deg * Math.PI / 180;
    },

    distance: function (x1, y1, x2, y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    },

    distanceSq: function (x1, y1, x2, y2) {
        return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    },

    randomNumber: function (min, max) {
        return min + (max - min) * Math.random();
    },

    randomInt: function (min, max) {
        return (min + (max - min) * Math.random()) | 0;
    },

    lerp: function(v1, v2, amount) {
        return v1 + (v2 - v1) * amount;
    }
});