define(['lib/class'], function (Class) {
    return Class.extend({
        init: function (seed) {
            this.m = 0x80000000;
            this.a = 1103515245;
            this.c = 12345;

            this.state = seed ? seed : +new Date;
        },

        nextInt: function () {
            return this.state = (this.a * this.state + this.c) % this.m;
        }
    });
})