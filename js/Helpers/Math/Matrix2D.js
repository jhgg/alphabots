define(['lib/class'], function (Class) {
    var Matrix2D = Class.extend({
        a: 1,
        b: 0,
        c: 0,
        d: 1,
        e: 0,
        f: 0,
        init: function (a, b, c, d, e, f) {
            this.setValues(a, b, c, d, e, f);
        },

        setValues: function (a, b, c, d, e, f) {
            if (a instanceof Matrix2D) {
                this.a = a.a;
                this.b = a.b;
                this.c = a.c;
                this.d = a.d;
                this.e = a.e;
                this.f = a.f;
            } else {
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
                this.e = e;
                this.f = f;
            }
        },

        clone: function () {
            return new Matrix2D(this);
        },

        apply: function (vector) {
            var x = vector.x,
                y = vector.y;

            vector.x = x * this.a + y * this.c + this.e;
            vector.y = x * this.b + y * this.d + this.f;

            return vector;
        },

        combine: function (other) {
            var tmp = this.a;
            this.a = tmp * other.a + this.b * other.c;
            this.b = tmp * other.b + this.b * other.d;
            tmp = this.c;
            this.c = tmp * other.a + this.d * other.c;
            this.d = tmp * other.b + this.d * other.d;
            tmp = this.e;
            this.e = tmp * other.a + this.f * other.c + other.e;
            this.f = tmp * other.b + this.f * other.d + other.f;
            return this;
        },

        equals: function (other) {
            return other instanceof Matrix2D &&
                this.a == other.a && this.b == other.b && this.c == other.c &&
                this.d == other.d && this.e == other.e && this.f == other.f;
        },

        determinant: function () {
            return this.a * this.d - this.b * this.c;
        },

        invert: function () {
            var d = this.determinant();

            if (d !== 0) {
                var old = this.asObj();

                this.a = old.d / det;
                this.b = -old.b / det;
                this.c = -old.c / det;
                this.d = old.a / det;
                this.e = (old.c * old.f - old.e * old.d) / det;
                this.f = (old.e * old.b - old.a * old.f) / det;

            }

            return this;
        },

        asObj: function () {
            return {
                a: this.a,
                b: this.b,
                c: this.c,
                d: this.d,
                e: this.e,
                f: this.f
            };
        },

        isIdentity: function () {
            return this.a === 1 && this.b === 0 && this.c === 0 && this.d === 1 && this.e === 0 && this.f === 0;
        },

        isInvertible: function () {
            return this.determinant() !== 0;
        },
        preRotate: function (rads) {
            var nCos = Math.cos(rads);
            var nSin = Math.sin(rads);

            var tmp = this.a;
            this.a = nCos * tmp - nSin * this.b;
            this.b = nSin * tmp + nCos * this.b;
            tmp = this.c;
            this.c = nCos * tmp - nSin * this.d;
            this.d = nSin * tmp + nCos * this.d;

            return this;
        },
        preScale: function (scalarX, scalarY) {
            if (scalarY === undefined)
                scalarY = scalarX;

            this.a *= scalarX;
            this.b *= scalarY;
            this.c *= scalarX;
            this.d *= scalarY;

            return this;
        },
        preTranslate: function (dx, dy) {
            if (typeof dx === "number") {
                this.e += dx;
                this.f += dy;
            } else {
                this.e += dx.x;
                this.f += dx.y;
            } // else

            return this;
        },
        rotate: function (rads) {
            var nCos = Math.cos(rads);
            var nSin = Math.sin(rads);

            var tmp = this.a;
            this.a = nCos * tmp - nSin * this.b;
            this.b = nSin * tmp + nCos * this.b;
            tmp = this.c;
            this.c = nCos * tmp - nSin * this.d;
            this.d = nSin * tmp + nCos * this.d;
            tmp = this.e;
            this.e = nCos * tmp - nSin * this.f;
            this.f = nSin * tmp + nCos * this.f;

            return this;
        },

        scale: function (scalarX, scalarY) {
            if (scalarY === undefined)
                scalarY = scalarX;

            this.a *= scalarX;
            this.b *= scalarY;
            this.c *= scalarX;
            this.d *= scalarY;
            this.e *= scalarX;
            this.f *= scalarY;

            return this;
        },
        toString: function () {
            return "Matrix2D([" + this.a + ", " + this.c + ", " + this.e +
                "] [" + this.b + ", " + this.d + ", " + this.f + "] [0, 0, 1])";
        },

        translate: function (dx, dy) {
            if (typeof dx === "number") {
                this.e += this.a * dx + this.c * dy;
                this.f += this.b * dx + this.d * dy;
            } else {
                this.e += this.a * dx.x + this.c * dx.y;
                this.f += this.b * dx.x + this.d * dx.y;
            }

            return this;
        }
    });
});