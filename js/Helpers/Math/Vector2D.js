define(['lib/class', 'Helpers/Math/Position'], function(Class, Position) {
    var Vector2D = Class.extend({
        x: 0,
        y: 0,
        init: function(x, y) {
            if (x instanceof Vector2D) {
                this.x = x.x;
                this.y = x.y;
            } else {
                this.x = x;
                this.y = y;
            }
        },

        add: function (other) {
            this.x += other.x;
            this.y += other.y;

            return this;
        },

        subtract: function (other) {
            this.x -= other.x;
            this.y -= other.y;

            return this;
        },

        angleBetween: function(other) {
            return Position.getAngle(this, other);
        },

        angleTo: function (other) {
            return Position.getAngleTo(this, other);
        },

        clone: function() {
            return new Vector2D(this.x, this.y);
        },

        distance: function(other) {
            return Position.getExactDist2d(this, other);
        },

        distanceSq: function(other) {
            return Position.getExactDist2dSq(this, other);
        },

        divide: function(other) {
            this.x /= other.x;
            this.y /= other.y;
            return this;
        },

        dotProduct: function(other) {
            return this.x * other.x + this.y * other.y;
        },

        equals: function(other) {
            return other instanceof Vector2D &&
                this.x == other.x && this.y == other.y;
        },

        getNormal: function(other) {
            if (other === undefined)
                return new Vector2D(-this.y, this.x);

            return new Vector2D(other.y - this.y, this.x - other.x).normalize();

        },

        isZero: function() {
            return this.x === 0 && this.y === 0;
        },

        magnitude: function() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        },

        magnitudeSq: function() {
            return this.x * this.x + this.y * this.y;
        },

        multiply: function(other) {
            this.x *= other.x;
            this.y *= other.y;
            return this;
        },

        negate: function(other) {
            this.x *= -1;
            this.y *= -1;

            return this;
        },

        normalize: function() {
            var magnitude = this.magnitude();

            if (magnitude === 0)  {
                this.x = 1;
                this.y = 0;
            } else {
                this.x /= magnitude;
                this.y /= magnitude;
            }

            return this;
        },

        scale: function(scaleX ,scaleY) {

            if (scaleY === undefined)
                scaleY = scaleX;

            this.x *= scaleX;
            this.y *= scaleY;

            return this;
        },

        scaleToMagnitude: function(magnitude) {
            return this.scale(magnitude / this.magnitude());
        },

        setValues: function(x, y) {
            if (x instanceof Vector2D) {
                this.x = x.x;
                this.y = x.y;
            } else {
                this.x = x;
                this.y = y;
            }

            return this;
        },

        translate: function(dx, dy) {
            if (dy === undefined)
                dx = dy;

            this.x += dx;
            this.y += dy;

            return this;
        },

        tripleProduct: function(a, b, c) {
            var ac = a.dotProduct(c);
            var bc = b.dotProduct(c);

            return new Vector2D(b.x * ac - a.x * bc, b.y * ac - a.y * bc);
        }

    });
    return Vector2D;
});