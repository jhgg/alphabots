define([] , function() {
    'use strict';
    var exports = {};

    exports.getAbsolutePositionOfCanvasSprite = function(sprite) {
        return exports.getAbsolutePositionOfCanvasItemxy(sprite._x, sprite._y);
    };

    exports.getAbsolutePositionOfCanvasItemxy = function (x, y) {
        var rect = window.$$canvas.getBoundingClientRect();

        return {
            _x: x + rect.left,
            _y: y + rect.top
        };
    };

    exports.getAngle = function(self, other) {
        var dx = self._x - other._x;
        var dy = self._y - other._y;

        var ang = Math.atan2(dy, dx);
        ang = (ang >= 0) ? ang : 2 * Math.PI + ang;
        return ang;

    };

    exports.getAngleTo = function(self, other) {
        var dx = self._x * other._y - self._y * other._x,
            dy = self._x * other._x + self._y * other._y;

        var ang = Math.atan2(dy, dx);
        ang = (ang >= 0) ? ang : 2 * Math.PI + ang;
        return ang;
    };

    exports.getExactDist2dSq = function(self, other) {
        var dx = self._x - other._x;
        var dy = self._y - other._y;

        return dx*dx + dy*dy;
    };

    exports.getExactDist2d = function(self, other) {
        var dx = self._x - other._x;
        var dy = self._y - other._y;

        return Math.sqrt(dx*dx + dy*dy);
    };

    exports.normalizeOrientation = function(o) {
        if (o < 0.0) {
            var mod = -o;

            mod %= Math.PI * 2.0;
            mod = -mod + 2.0 * Math.PI;
            return mod;
        }

        return o % (2.0 * Math.PI);

    };

    exports.hasInArc = function(self, other, arc, border) {
        if (self === other)
            return true; // always have the same object in arc.

        if (border === undefined)
            border = 2.0;

        var arc = exports.normalizeOrientation(arc),
            angle = exports.getAngle(self, other);

        angle -= self.A;
        angle = exports.normalizeOrientation(angle);

        if (angle > Math.PI)
            angle -= 2.0 * Math.PI;

        var lborder = -1 * (arc/border);
        var rborder = (arc/border);
        return ((angle >= lborder) && (angle <= rborder));

    };

    exports.hasInFront = function(self, other) {
        return exports.hasInArc(self, other, Math.PI);
    };

    exports.hasInBack = function(self, other) {
        return exports.hasInArc(self, other, -Math.PI);
    };

    exports.getRandomPointNear = function(self, other, distance) {
        if (!distance)
            return {
                _x: self._x,
                _y: self._y
            };


        var angle = Math.random() * Math.PI;
        var newDistance = Math.random() * distance;

        return {
            _x: self._x + newDistance * Math.cos(angle),
            _y: self._y + newDistance * Math.sin(angle)
        };
    };

    exports.newPointFromAdding = function(self, other) {
        return {
            _x: self._x + other._x,
            _y: self._y + other._y
        };
    };

    exports.randomPointOnCanvas = function() {
        return {
            _x: Math.random() * $$game.canvasWidth | 0,
            _y: Math.random() * $$game.canvasHeight | 0
        };
    };

    return exports;

});