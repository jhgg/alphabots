define(function () {
    var pool = [],
        pointer = 0;
    return {
        get: function (x, y, w, h) {
            if (pool.length <= pointer)
                pool.push({});
            var r = pool[pointer++];
            r._x = x;
            r._y = y;
            r._w = w;
            r._h = h;
            return r;
        },

        copy: function (o) {
            if (pool.length <= pointer)
                pool.push({});
            var r = pool[pointer++];
            r._x = o._x;
            r._y = o._y;
            r._w = o._w;
            r._h = o._h;
            return r;
        },

        recycle: function (o) {
            pointer--;
        }
    };

});