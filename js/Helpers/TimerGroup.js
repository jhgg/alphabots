define(['lib/class'], function(Class) {
    return Class.extend({
        init: function() {
            self.timers = {};
            self.loops = {};
        },

        delay: function(fn, msec) {
            var id = setTimeout(function() {
                delete self.timers[id];
                fn();
            }, msec)
            self.timers[id] = 1;
            return id;
        },

        loop: function(fn, msec, right_away) {
            right_away && setTimeout(fn, 0);
            var id = setInterval(fn, msec);
            self.loops[id] = 1;
            return id;
        },
        cancelAll: function() {
            for(var key in self.timers)
                clearTimeout(key);
            self.timers = {};

            for(key in self.loops)
                clearInterval(key);

            self.loops = {};
        },

        cancelDelay: function(id) {
            delete self.timers[id];
            clearTimeout(id);
        },

        cancelLoop: function(id) {
            delete self.loops[id];
            clearInterval(id);
        }

    })

});