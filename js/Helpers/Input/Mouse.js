define(['require'], function (require) {
    var elem;

    function onMouseDown(event) {
        exports.mouseDown = true;
    }

    function onMouseUp(event) {
        exports.mouseDown = false;
    }

    function onMouseMove(e) {
        var movementX = e.movementX ||
                e.mozMovementX ||
                e.webkitMovementX ||
                0,
            movementY = e.movementY ||
                e.mozMovementY ||
                e.webkitMovementY ||
                0;

        // Print the mouse movement delta values
        exports.position._x = e.pageX;
        exports.position._y = e.pageY;
        exports.deltas._x += movementX;
        exports.deltas._y += movementY;
        exports.deltas.updated = true;
    }

    function fullscreenChange() {
        if (document.webkitFullscreenElement === elem ||
            document.mozFullscreenElement === elem ||
            document.mozFullScreenElement === elem) { // Older API upper case 'S'.
            // Element is fullscreen, now we can request pointer lock
            elem.requestPointerLock = elem.requestPointerLock ||
                elem.mozRequestPointerLock ||
                elem.webkitRequestPointerLock;
            elem.requestPointerLock();
        }
    }


    function pointerLockChange() {
        if (document.mozPointerLockElement === elem ||
            document.webkitPointerLockElement === elem) {
            console.log("Pointer Lock was successful.");
        } else {
            console.log("Pointer Lock was lost.");
        }
    }


    function pointerLockError() {
        console.log("Error while locking pointer.");
    }

    function onSelectStart(event) {
        return false;
    }

    function doFullScreen() {
        elem = require('Game').getGameContainer();
        // Start by going fullscreen with the element.  Current implementations
        // require the element to be in fullscreen before requesting pointer
        // lock--something that will likely change in the future.
        elem.requestFullscreen = elem.requestFullscreen ||
            elem.mozRequestFullscreen ||
            elem.mozRequestFullScreen || // Older API upper case 'S'.
            elem.webkitRequestFullscreen;
        elem.requestFullscreen();
    }

    var exports = {
        deltas: {
            _x: 0,
            _y: 0,
            updated: false
        },
        position: {
            _x: 0,
            _y: 0
        },
        mousedown: false
    };

    exports.init = function () {
        document.addEventListener('fullscreenchange', fullscreenChange, false);
        document.addEventListener('mozfullscreenchange', fullscreenChange, false);
        document.addEventListener('webkitfullscreenchange', fullscreenChange, false);

        document.addEventListener('pointerlockerror', pointerLockError, false);
        document.addEventListener('mozpointerlockerror', pointerLockError, false);
        document.addEventListener('webkitpointerlockerror', pointerLockError, false);

        document.addEventListener('pointerlockchange', pointerLockChange, false);
        document.addEventListener('mozpointerlockchange', pointerLockChange, false);
        document.addEventListener('webkitpointerlockchange', pointerLockChange, false);

        document.addEventListener("mousemove", onMouseMove);
        document.addEventListener("mousedown", onMouseDown);
        document.addEventListener("mouseup", onMouseUp);
        document.addEventListener("selectstart", onSelectStart);

    };

    exports.flushDeltas = function() {
        exports.deltas.x = 0;
        exports.deltas.y = 0;
        exports.deltas.updated = false;
    };

    exports.subtractDeltasXY = function(x, y) {
        exports.deltas.x -= x;
        exports.deltas.y -= y;
    };

    exports.subtractDeltas = function(position) {
        exports.subtractDeltasXY(position.x, position.y)
    };


    exports.getDeltas = function() {
        return exports.deltas;
    };

    exports.isDeltasUpdated = function() {
        return exports.deltas.updated;
    };

    exports.registerFullscreenButton = function(button) {
        button.addEventListener('click', doFullScreen, true);
    };


    return exports;
});