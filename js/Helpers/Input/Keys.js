define(function () {
    "use strict";

    var keys_pressed = new Array(256);

    window.addEventListener('keydown', function (e) {
        keys_pressed[e.keyCode] = true;
    }, false);
    window.addEventListener('keyup', function (e) {
        keys_pressed[e.keyCode] = false;
    }, false);

    return {
        isKeyPressed: function (k) {
            return keys_pressed[k];
        },
        keys: {
            LEFT_ARROW: 37,
            UP_ARROW: 38,
            RIGHT_ARROW: 39,
            DOWN_ARROW: 40,
            SPACE: 32
        }
    }

});