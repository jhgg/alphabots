// Sprite2... Inspired by CraftyJS.

define(['Base/EventEmitter', 'Collision/HashMap', 'Helpers/RectPool', 'Helpers/Math/Position'],
    function (EventEmitter, HashMap, RectPool, Position) {
        var DEG_TO_RAD = Math.PI / 180;

        return EventEmitter.extend({
            _x: 0,
            _y: 0,
            _w: 0,
            _h: 0,
            _z: 0,
//        _flipX: false,
//        _flipY: false,
            _visible: true,
            _alpha: 1,
            _rotation: 0,
            _parent: null,
            _children: null,
            _mbr: null,
            _entry: null,
            _globalZ: null,
            map: null,
            mapArea: null,
            requires: null,
            sprite: null,

            init: function () {
                this._super();
                this._globalZ = this[0];

                this._children = [];
                this._origin = { x: 0, y: 0 };
                this._entry = HashMap.insert(this);

                this.on("move", this._update_entry_and_children);
                this.on("rotate", this._update_entry_and_children);
                this.on("destroy", this._reap_children);

            },

            // Destructor.
            destroy: function () {
                HashMap.remove(this);
                this._super();
            },

            // Properties
            __get_x: function () {
                return this._x;
            },
            __set_x: function (val) {
                this._set('_x', val);
            },
            __get_y: function () {
                return this._y;
            },
            __set_y: function (val) {
                this._set('_y', val);
            },
            __get_w: function () {
                return this._w;
            },
            __set_w: function (val) {
                this._set('_w', val);
            },
            __get_h: function () {
                return this._h;
            },
            __set_h: function (val) {
                this._set('_h', val);
            },
            __get_z: function () {
                return this._z;
            },
            __set_z: function (val) {
                this._set('_z', val);
            },
            __get_alpha: function () {
                return this._alpha;
            },
            __set_alpha: function (val) {
                this._set('_alpha', val);
            },
            __get_visible: function () {
                return this._visible;
            },
            __set_visible: function (val) {
                this._set('_visible', val);
            },
            __get_rotation: function () {
                return this._rotation;
            },
            __set_rotation: function (val) {
                this._set('_rotation', val);
            },

            __get_numChildren: function () {
                return this._children.length;
            },


            // Property Watcher
            _set: function (name, val) {
                if (this[name] === val)
                    return;

                var old = RectPool.copy(this);

                switch (name) {
                    case '_rotation':
                        this._rotate(val);
                        this[name] = val;
                        break;

                    case '_z':
                        // Far better than crafty's method of zero padding.
                        this._globalZ = this[0] | val << 17;
                        break;

                    case '_x':
                    case '_y':
                        var mbr;
                        if (mbr = this._mbr) {
                            mbr[name] -= this[name] - val;
                        }
                        this[name] = val;

                        this.trigger('move', old);
                        break;

                    case '_h':
                    case '_w':
                        this._mbr && this._calculateMBR(this._origin.x + this._x, this._origin.y + this._y,
                            -this._rotation * DEG_TO_RAD);

                        this[name] = val;

                        this.trigger('resize', {
                            axis: name[1],
                            amount: val - old[name]
                        });

                        this.trigger('move', old);
                        break;

                    default:
                        this[name] = val;
                }

                this.trigger("change", old);
                RectPool.recycle(old);

            },

            // Calculating MBR - based upon craftyjs.
            _calculateMBR: function (ox, oy, rad) {
                if (rad == 0) {
                    this._mbr = null;
                    return;
                }

                var ct = Math.cos(rad),
                    st = Math.sin(rad);
                // Special case 90 degree rotations to prevent rounding problems
                ct = (ct < 1e-10 && ct > -1e-10) ? 0 : ct;
                st = (st < 1e-10 && st > -1e-10) ? 0 : st;
                var x0 = ox + (this._x - ox) * ct + (this._y - oy) * st,
                    y0 = oy - (this._x - ox) * st + (this._y - oy) * ct,
                    x1 = ox + (this._x + this._w - ox) * ct + (this._y - oy) * st,
                    y1 = oy - (this._x + this._w - ox) * st + (this._y - oy) * ct,
                    x2 = ox + (this._x + this._w - ox) * ct + (this._y + this._h - oy) * st,
                    y2 = oy - (this._x + this._w - ox) * st + (this._y + this._h - oy) * ct,
                    x3 = ox + (this._x - ox) * ct + (this._y + this._h - oy) * st,
                    y3 = oy - (this._x - ox) * st + (this._y + this._h - oy) * ct,
                    minx = Math.floor(Math.min(x0, x1, x2, x3)),
                    miny = Math.floor(Math.min(y0, y1, y2, y3)),
                    maxx = Math.ceil(Math.max(x0, x1, x2, x3)),
                    maxy = Math.ceil(Math.max(y0, y1, y2, y3));
                if (!this._mbr) {
                    this._mbr = {
                        _x: minx,
                        _y: miny,
                        _w: maxx - minx,
                        _h: maxy - miny
                    };
                } else {
                    this._mbr._x = minx;
                    this._mbr._y = miny;
                    this._mbr._w = maxx - minx;
                    this._mbr._h = maxy - miny;
                }

            },
            rotate: function (e) {
                //assume event data origin
                this._origin.x = e.o.x - this._x;
                this._origin.y = e.o.y - this._y;

                //modify through the setter method
                this._set('_rotation', this._rotation - e.deg);
            },
            moveToRandomPointOnCanvas: function() {
                var pos = Position.randomPointOnCanvas();
                this.x = pos._x;
                this.y = pos._y;
            },
            _rotate: function (v) {
                var theta = -1 * (v % 360); //angle always between 0 and 359
                var difference = this._rotation - v;
                // skip if there's no rotation!
                if (difference == 0)
                    return;

                //Calculate the new MBR
                var rad = theta * DEG_TO_RAD,
                    o = {
                        x: this._origin.x + this._x,
                        y: this._origin.y + this._y
                    };

                this._calculateMBR(o.x, o.y, rad);


                //trigger "Rotate" event
                var drad = difference * DEG_TO_RAD,
                    ct = Math.cos(rad),
                    st = Math.sin(rad);

                this.trigger("rotate", {
                    cos: Math.cos(drad),
                    sin: Math.sin(drad),
                    deg: difference,
                    rad: drad,
                    o: o,
                    matrix: {
                        M11: ct,
                        M12: st,
                        M21: -st,
                        M22: ct
                    }
                });
            },

            intersect: function (x, y, w, h) {
                var rect, mbr = this._mbr || this;
                if (typeof x === "object") {
                    rect = x;
                } else {
                    rect = {
                        x: x,
                        y: y,
                        w: w,
                        h: h
                    };
                }

                return mbr._x < rect.x + rect.w && mbr._x + mbr._w > rect.x &&
                    mbr._y < rect.y + rect.h && mbr._h + mbr._y > rect.y;
            },
            within: function (x, y, w, h) {
                var rect, mbr = this._mbr || this;
                if (typeof x === "object") {
                    rect = x;
                } else {
                    rect = {
                        _x: x,
                        _y: y,
                        _w: w,
                        _h: h
                    };
                }

                return rect._x <= mbr._x && rect._x + rect._w >= mbr._x + mbr._w &&
                    rect._y <= mbr._y && rect._y + rect._h >= mbr._y + mbr._h;
            },

            contains: function (x, y, w, h) {
                var rect, mbr = this._mbr || this;
                if (typeof x === "object") {
                    rect = x;
                } else {
                    rect = {
                        _x: x,
                        _y: y,
                        _w: w,
                        _h: h
                    };
                }

                return rect._x >= mbr._x && rect._x + rect._w <= mbr._x + mbr._w &&
                    rect._y >= mbr._y && rect._y + rect._h <= mbr._y + mbr._h;
            },

            pos: function () {
                return {
                    _x: (this._x),
                    _y: (this._y),
                    _w: (this._w),
                    _h: (this._h)
                };
            },

            mbr: function () {
                if (!this._mbr) return this.pos();
                return {
                    _x: (this._mbr._x),
                    _y: (this._mbr._y),
                    _w: (this._mbr._w),
                    _h: (this._mbr._h)
                };
            },

            isAt: function (x, y) {
                if (this.mapArea) {
                    return this.mapArea.containsPoint(x, y);
                } else if (this.map) {
                    return this.map.containsPoint(x, y);
                }
                var mbr = this._mbr || this;
                return mbr._x <= x && mbr._x + mbr._w >= x &&
                    mbr._y <= y && mbr._y + mbr._h >= y;
            },

            shift: function (x, y, w, h) {
                if (x) this.x += x;
                if (y) this.y += y;
                if (w) this.w += w;
                if (h) this.h += h;

                return this;
            },

            centerOrigin: function (center) {
                if (center) {
                    this._origin.x = this._w / 2;
                    this._origin.y = this._h / 2;
                } else {
                    this._origin.x = 0;
                    this._origin.y = 0;
                }
            },

            // Children Functions
            addChild: function () {
                var i = 0,
                    new_children = arguments, new_children__length = new_children.length,
                    new_child;

                for (; i < new_children__length; i++) {
                    new_child = new_children[i];
                    if (new_child._parent) {
                        new_child._parent.removeChild(new_child);
                    }
                    new_child._parent = this;
                    this._children.push(new_child);
                }

                return this;
            },

            removeChild: function (entity) {
                var children = this._children,
                    children_length = this._children.length,
                    i;

                if (!entity) {
                    for (i = 0; i < children_length; i++)
                        delete children[i]._parent;

                    this._children.length = 0;

                    return this;
                }
                for (i = 0; i < children_length; i++) {
                    if (children[i] === entity) {
                        i--;
                        children_length--;
                        children.splice(i, 1);
                    }
                }

                entity._parent = null;
                return this;
            },

            // Private Children Functions
            _move_children: function (e) {
                if (!e) return;

                var i, children = this._children,
                    children__length = children.length,
                    child;

                if (e.cos) {
                    for (i = 0; i < children__length; i++) {
                        child = children[i];
                        ('rotate' in child) && child.rotate(e);
                    }
                } else {
                    var dx = this._x - e._x,
                        dy = this._y - e._y,
                        dw = this._w - e._w,
                        dh = this._h - e._h;

                    for (i = 0; i < children__length; ++i) {
                        child = children[i];
                        child.shift(dx, dy, dw, dh);
                    }
                }
            },
            _update_entry_and_children: function (e) {
                this._entry.update(this._mbr || this);
                this._move_children(e);
            },

            _reap_children: function () {
                if (this._children) {
                    var children = this._children,
                        children__length = this._children.length,
                        i, child;

                    for (i = 0; i < children__length; i++) {
                        child = children[i];
                        delete child._parent;
                        child.destroy && child.destroy();
                    }
                    this._children.length = 0;
                }

                if (this._parent) {
                    this._parent.detach(this);
                    this._parent = null;
                }
            }
        });
    });