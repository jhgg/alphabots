define(['Base/Object', 'Game/EntityManager'], function (Object, EntityManager) {
    var handlers = EntityManager.handlers;

    return Object.extend({
        // Register a callback to this event.
        on: function (event, callback) {
            if (!handlers[event]) handlers[event] = {};

            var eventHandler = handlers[event],
                guid = this[0];

            if (!eventHandler[guid])
                eventHandler[guid] = [];

            eventHandler[guid].push(callback);

            return this;
        },

        // Register a callback to an event that only gets fired once.
        one: function(event, callback) {
            var self = this;
            self.on(event, function(data) {
                callback.call(self, data);
                this.off(event,callback);
            });
        },

        // Deregister a calblback from an event. If no callback is provided, removes all callbacks.
        off: function (event, callback) {
            var eventHandlers = handlers[event],
                eventHandlers_length, current,
                guid = this[0], num_deleted_handlers = 0;

            if (eventHandlers && eventHandlers[guid])
                eventHandlers_length = eventHandlers[guid].length;
            else
                return this;

            if (!callback) {
                delete eventHandlers[guid];
                return this;
            } else {
                eventHandlers = eventHandlers[guid];
            }


            for (var i = 0; i < eventHandlers_length; i++) {
                if (eventHandlers[i] === callback) {
                    delete eventHandlers[i];
                    num_deleted_handlers++;
                }
            }

            if (num_deleted_handlers == eventHandlers_length) {
                delete eventHandlers[guid];
            }

            return this;
        },

        uniqueOn: function (event, callback) {
            this.off(event, callback);
            this.on(event, callback);
        },

        trigger: function (event, data) {
            var guid = this[0],
                eventHandlers = handlers[event];

            if (eventHandlers && eventHandlers[guid]) {
                var callbacks = eventHandlers[guid],
                    callbacks_length = callbacks.length;

                for (var i = 0; i < callbacks_length; i++) {
                    if (callbacks[i] === undefined) {
                        callbacks.splice(i, 1);
                        callbacks_length--;
                        i--;
                    } else {
                        callbacks[i].call(this, data);
                    }
                }
            }
            return this;
        },

        destroy: function() {
            this._super();
            this.trigger('destroy');
            for (var event in handlers)
                this.off(event);
        }
    });

});

