define(['lib/class', 'Game/EntityManager'], function (Class, EntityManager) {
    "use strict";


    return Class.extend({
        _is_object: true, // do not touch.

        init: function () {
            this[0] = EntityManager.generateGUID();
            EntityManager.addEntity(this);

            this.isActive = true;
        },
        destroy: function() {
            EntityManager.removeEntity(this);
        },

        changeState: function(state) {
            this.state = state;
        }
    })
});