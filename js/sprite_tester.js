define(['lib/jquery'], function($) {
    var canvas = $('#canvas').get(0);
    var ctx = canvas.getContext('2d');
    var preview = $("#preview").get(0);
    var ctx2 = preview.getContext('2d');
    var to = null;

    $("#inputs").submit(function(e) {
        e && e.preventDefault();
        var $inputs = $(this).find('input'),
            data = {};

        $inputs.each(function(i, v) {
            var $v = $(v);
            data[$v.attr('name')] = $v.val();
        });

        data['image_url'] = 'dance.png';

        var img_src = data['image_url'];
        var rects = [];

        for(var y = +data.top_y; y < +data.bottom_y; y += +data.sprite_height) {
            for(var x = +data.top_x; x < +data.bottom_x; x += +data.sprite_width) {
                rects.push({
                    x: x,
                    y: y,
                    w: data.sprite_width,
                    h: data.sprite_height
                });
            }
        }

        rects.length = +data.no_slides;

        var img = new Image();
        var cur_slide = 0;
        to && clearInterval(to);
        img.src = img_src + "?bust=" + Date.now();
        img.onload = function() {
            canvas.width = img.width;
            canvas.height = img.height;
            preview.width = data.sprite_width;
            preview.height = data.sprite_height;
            ctx.drawImage(img, 0, 0);

            ctx.strokeStyle = 'red';

            $.each(rects, function(i, v) {
                ctx.strokeRect(v.x, v.y, v.w, v.h);
            });

            to = setInterval(function() {
                var crect = rects[cur_slide];
                ctx2.clearRect(0, 0, preview.width, preview.height);
                ctx2.drawImage(img, +crect.x, +crect.y, +crect.w, +crect.h, 0, 0, +crect.w, +crect.h);

                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0);

                ctx.strokeStyle = 'red';

                $.each(rects, function(i, v) {
                    ctx.strokeRect(v.x, v.y, v.w, v.h);
                });

                ctx.save();
                ctx.strokeStyle = 'limegreen';
                ctx.lineWidth = 7;
                ctx.strokeRect(crect.x, crect.y, crect.w, crect.h);
                ctx.restore();
                $("#cs").text(cur_slide + 1);

                cur_slide++;
                if (cur_slide >= rects.length) cur_slide = 0;

            }, 1000 / (+data.fps || 15));

        };
        console.log(data);
    });

});