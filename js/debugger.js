require.config({
    urlArgs: "bust=" + (new Date()).getTime(),


});
require(['lib/smoothie', 'lib/underscore'], function(Smoothie, _) {
    var series_list = ['fps', 'dps', 'entities_drawn', 'draw_time', 'frame_time'];
    var series = {};

    var charts = {
        'g_fps': {
            'series': ['fps'],
            'delay': 500
        },
        'g_dps': {
            'series': ['dps'],
            'delay': 500
        },
        'g_entities_drawn': {
            'series': ['entities_drawn'],
            'delay': 500
        },
        'g_draw_time': {
            'series': ['draw_time'],
            'delay': 500
        },
        'g_frame_time': {
            'series': ['frame_time'],
            'delay': 500
        }
    };

    _.forEach(series_list, function(v) {
        if (series[v])
            return;
        series[v] = new Smoothie.TimeSeries();
    });


    _.forEach(charts, function(v, k) {
        v.smoothie = new Smoothie.SmoothieChart();
        v.smoothie.streamTo(document.getElementById(k), v.delay);
        v.series && _.forEach(v.series, function(s) {
            v.smoothie.addTimeSeries(series[s]);
        });
    });

    window.addEventListener("message", function() {
        if (event.origin != document.location.origin)
            return;

        var s = series[event.data.series];
        s && s.append(event.data.time, event.data.value);
        console.log(event.data);

    }, false);


});