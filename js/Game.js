define(['Assets', 'Helpers/Color', 'Helpers/Input/Mouse',
        'lib/keypress', 'Game/MainLoop', 'Config'],
    function (Assets, Color, Mouse, keypress, MainLoop, Config) {
        "use strict";

        var ctx,
            canvas,
            gameIsInit = false;

        function clear() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        }

        function drawLoadingScreen(percentage) {
            clear();
            ctx.save();

            var mid_x = canvas.width / 2;
            var mid_y = canvas.height / 2;
            var progress_bar_width = 200;
            var progress_bar_height = 50;
            var progress_bar_padding = 5;

            ctx.fillStyle = '#FFFFFF';

            ctx.fillRect(mid_x - progress_bar_width / 2, mid_y - progress_bar_height / 2,
                progress_bar_width, progress_bar_height);

            ctx.fillStyle = '#FF0000';
            ctx.fillRect(mid_x - progress_bar_width / 2 + progress_bar_padding,
                mid_y - progress_bar_height / 2 + progress_bar_padding,
                (progress_bar_width - progress_bar_padding * 2) * percentage,
                progress_bar_height - progress_bar_padding * 2);

            ctx.restore();

        }

        function assetsLoaded() {
            console.log("Assets loaded");

            require(['Alphabots/Main'], function (Main) {
                Main();
                clear();
                MainLoop.start();
            });
        }

        function initGame() {
            if (gameIsInit)
                return;

            gameIsInit = true;
            Mouse.init();
            Mouse.registerFullscreenButton(document.getElementById('request_full_screen'));

            canvas = document.createElement('canvas');
            canvas.width = Config.canvas.width;
            canvas.height = Config.canvas.height;

            getGameStage().appendChild(canvas);
            window.$$canvas = canvas;
            ctx = canvas.getContext('2d');
            ctx.save();

            exports.canvas = canvas;
            exports.ctx = ctx;
            MainLoop.setCanvas(canvas, ctx);

            // Load the assets, and callback with "assetsLoaded" once everything's done
            // to start the main loop.
            drawLoadingScreen(0);
            Assets.load(assetsLoaded, drawLoadingScreen);
        }

        function getGameContainer() {
            return document.getElementById('game-container');
        }

        function getGameStage() {
            return document.getElementById('canvas_holder');
        }


        var exports = {
            init: initGame,
            getGameContainer: getGameContainer,
            getGameStage: getGameStage,
            canvasWidth: Config.canvas.width,
            canvasHeight: Config.canvas.height
        };

        window.$$game = exports;

        return exports;

    }
)
;
