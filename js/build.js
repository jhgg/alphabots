({
    appDir: "../",
    baseUrl: "js",
    dir: "../../Alphabots-Dist",
    modules: [
        {
            name: "main"
        },
        {
            name: "debugger"
        },
        {
            name: 'sprite_tester'
        }
    ],
    skipDirOptimize: true,
    removeCombined: true,
    preserveLicenseComments: false

})