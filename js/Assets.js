define(["Helpers/Loaders/ImageLoader", "Helpers/Loaders/BufferLoader", 'lib/underscore', 'Audio/Mixer', 'Config'],
function (ImageLoader, BufferLoader, _, Mixer, Config) {
    "use strict";

    var i = 0;
    var context = Mixer.context;

    return {
        sprites: {}, // populated by load function
        audio: {},
        music: {},

        // Convenience function for playing a sound.
        play_sound: function (buffer) {
            var source = context.createBufferSource();
            source.buffer = buffer;
            source.connect(context.destination);
            source.start(0);
        },

        play_audio: function (audio) {
            var source = context.createMediaElementSource(audio);
            source.connect(context.destination);
            audio.play();
        },

        play_sound_by_name: function (name) {
            var buffer = this.audio[name];
            if (!buffer)
                console.log("Audio by name", name, "was not found.");
            else
                this.play_sound(buffer);
        },
        play_sound_by_name_with_volume: function (name, volume) {
            var buffer = this.audio[name];
            if (!buffer)
                console.log("Audio by name", name, "was not found.");
            else
                this.play_sound_with_volume(buffer, volume);
        },
        // Convenience function for playing a sound with a specific volume.
        play_sound_with_volume: function (buffer, volume) {
            var source = context.createBufferSource(),
                gainNode = context.createGain();

            source.buffer = buffer;
            source.connect(gainNode);
            gainNode.connect(context.destination);
            gainNode.gain.value = volume;

            source.start(0);

            // Returns a function that allows you to set the volume of the sound...
            return function (vol) {
                gainNode.gain.value = vol;
            }
        },

        // Load all assets.
        load: function (all_assets_loaded, progress) {
            var that = this,
                total_assets_to_load = _.size(Config.assets.audio) + _.size(Config.assets.sprites),
                total_loaded = 0;

            function progress_inner() {
                total_loaded++;
                progress && progress(total_loaded / total_assets_to_load);
            }


            ImageLoader.loadImagesMap(Config.assets.sprites, function (sprites) {
                that.sprites = sprites;
                if (++i == 2)
                    all_assets_loaded();
            }, progress_inner);

            BufferLoader.loadBufferMap(context, Config.assets.audio, function (audio) {
                that.audio = audio;
                if (++i == 2)
                    all_assets_loaded();
            }, progress_inner);

            _.each(Config.assets.music, function (v, k) {
                that.music[k] = new Audio(v);
            })

        }
    };
});