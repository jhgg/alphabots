// Yet again, more functionality inspired by Crafty.

define(['require', 'Config', 'Collision/HashMap', 'Game/MainLoop', 'Game/GlobalEvents', 'Debugger/Canvas'],
function (require, Config, HashMap, MainLoop, GlobalEvents, DebugCanvas) {
    'use strict';
    var EntityManager;
    // patch in circular dependency // hACK!
    require(['Game/MainLoop', 'Game/EntityManager'], function (l, m) {
        MainLoop = l;
        EntityManager = m;
    });

    var dirty_rect_list = [],   // list of dirty rectangles to update.
        changed_entities = [];   // list of dirty objects.

    // Function Aliases
    var Math__max = Math.max,
        Math__min = Math.min;

    // Convenience Vars
    var canvas_rect = {
        _w: Config.canvas.width,
        _h: Config.canvas.height,
        _x: 0,
        _y: 0
    };

    var redraw_factor = Config.canvas.redraw_factor || 0.6;


    // Sort objects by their Z index.
    function zsort(a, b) {
        return a._globalZ - b._globalZ;
    }

    function rect__merge(a, b, target) {
        if (target === undefined)
            target = {};

        target._h =  Math__max(a._y + a._h, b._y + b._h);
        target._w =  Math__max(a._x + a._w, b._x + b._w);
        target._x =  Math__min(a._x, b._x) | 0;
        target._y =  Math__min(a._y, b._y) | 0;
        target._w -= target._x;
        target._h -= target._y;
        target._w = (target._w == target._w | 0) ? target._w : ~~target._w + 1 | 0;
        target._h = (target._h == target._h | 0) ? target._h : ~~target._h + 1 | 0;
        return target
    }

    function rect__clean() {
        var dirty_rect, obj, i, l;

        for (i = 0, l = changed_entities.length; i < l; i++) {
            obj = changed_entities[i];
            dirty_rect = obj._mbr || obj;
            if (obj.staleRect == null)
                obj.staleRect = {};

            obj.staleRect._x = dirty_rect._x;
            obj.staleRect._y = dirty_rect._y;
            obj.staleRect._w = dirty_rect._w;
            obj.staleRect._h = dirty_rect._h;

            obj._changed = false

        }

        changed_entities.length = 0;
        dirty_rect_list.length = 0;
    }

    function rect__dirty(obj) {
        var dirty_rect = obj._mbr || obj;
        if (obj.staleRect) {
            // if they overlap, merge them into one bigger rectangle.
            if (rect__overlap(obj.staleRect, dirty_rect)) {
                dirty_rect_list.push(rect__merge(obj.staleRect, dirty_rect, obj.staleRect));
                return;
            }
            dirty_rect_list.push(obj.staleRect);
        }
        if (obj.currentRect == null)
            obj.currentRect = {};

        obj.currentRect._x = dirty_rect._x;
        obj.currentRect._y = dirty_rect._y;
        obj.currentRect._w = dirty_rect._w;
        obj.currentRect._h = dirty_rect._h;
        dirty_rect_list.push(obj.currentRect)
    }

    function rect__overlap(a, b) {
        return (a._x < b._x + b._w
             && a._y < b._y + b._h
             && a._x + a._w > b._x
             && a._y + a._h > b._y);
    }

    function mergeSet(rect_set) {
        var i = 0;
        while (i < rect_set.length - 1) {
            if (rect__overlap(rect_set[i], rect_set[i + 1])) {
                rect__merge(rect_set[i], rect_set[i + 1], rect_set[i]);
                rect_set.splice(i + 1, 1);
                if (i > 0) i--;
            } else {
                i++;
            }
        }
    }

    function drawDebugPolygon(ctx, current_entity) {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = "orange";
        ctx.lineWidth = 1;
        var points = current_entity.map.points;
        ctx.moveTo(points[0][0], points[0][1]);
        for (var q = 1, l = points.length; q < l; q++)
            ctx.lineTo(points[q][0], points[q][1]);
        ctx.lineTo(points[0][0], points[0][1]);
        ctx.stroke();
        ctx.restore();
    }

    return {
        addEntity: function (entity) {
            changed_entities.push(entity);
        },

        drawAll: function (dirty_rect) {
            dirty_rect = dirty_rect || canvas_rect;

            var entities = HashMap.search(dirty_rect),
                i, entities_length, ctx = MainLoop.getContext(),
                entity, __debug_ctx;

            if ((Config.canvas.debug_mbr | Config.canvas.debug_entity_maps) && !__debug_ctx) {
                __debug_ctx = DebugCanvas.getDebugContext();
                DebugCanvas.clearDebugCanvas();
            }

            // Clear the screen.
            ctx.clearRect(dirty_rect._x, dirty_rect._y, dirty_rect._w, dirty_rect._h);

            // Sort entities by their Z index, before drawing.
            entities.sort(zsort);

            for (i = 0, entities_length = entities.length; i < entities_length; i++) {
                entity = entities[i];
                if (!entity._visible)
                    continue;

                entity.draw(ctx);

                if (Config.canvas.debug_entity_maps && entity.map && entity.map.points)
                    drawDebugPolygon(__debug_ctx, entity);

                if (Config.canvas.debug_mbr) {
                    if (!entity._mbr || Config.canvas.debug_both_rects) {
                        __debug_ctx.strokeStyle = entity._mbr ? 'gray' : 'green';
                        __debug_ctx.strokeRect(entity._x, entity._y, entity._w, entity._h);
                    }

                    if (entity._mbr) {
                        __debug_ctx.strokeStyle = 'red';
                        __debug_ctx.strokeRect(entity._mbr._x, entity._mbr._y, entity._mbr._w, entity._mbr._h);
                    }
                }

                entity._changed = false;
            }
        },

        boundingRect: function (rect_set) {
            if (!rect_set || !rect_set.length) return;
            var i = 1, rect_set_length = rect_set.length, current, master = rect_set[0],
                tmp_0, tmp_1, tmp_2, tmp_3,
                master_0 = master._x,
                master_1 = master._y,
                master_2 = master._x + master._w,
                master_3 = master._y + master._h;

            for (; i < rect_set_length; i++) {
                current = rect_set[i];
                tmp_0 = current._x;
                tmp_1 = current._y;
                tmp_2 = current._x + current._h;
                tmp_3 = current._y + current._w;

                if (tmp_0 < master_0) master_0 = tmp_0;
                if (tmp_1 < master_1) master_1 = tmp_1;
                if (tmp_2 > master_2) master_2 = tmp_2;
                if (tmp_3 > master_3) master_3 = tmp_3;
            }
            master = { _x: master_0, _y: master_1, _w: master_2 - master_0, _h: master_3 - master_1 };
            return master;
        },

        draw: function (ctx) {

            // nothing to draw here.
            if (!changed_entities.length)
                return;

            var i, j,
                changed_entities__length = changed_entities.length,
                dirty_rect, entity_list, entity_list__length, entity, entity__mbr;


            if (changed_entities__length / EntityManager.num_entities > redraw_factor) {
                this.drawAll();
                rect__clean();
                return;
            }

            for (i = 0; i < changed_entities__length; i++)
                rect__dirty(changed_entities[i]);


            // unrolled merge_set.
            i = 0;
            while (i < dirty_rect_list.length - 1) {
                // Unrolled rect__overlap
                if (dirty_rect_list[i]._x < dirty_rect_list[i + 1]._x + dirty_rect_list[i + 1]._w
                    && dirty_rect_list[i]._y < dirty_rect_list[i + 1]._y + dirty_rect_list[i + 1]._h
                    && dirty_rect_list[i]._x + dirty_rect_list[i    ]._w > dirty_rect_list[i + 1]._x
                    && dirty_rect_list[i]._y + dirty_rect_list[i    ]._h > dirty_rect_list[i + 1]._y) {

                    rect__merge(dirty_rect_list[i], dirty_rect_list[i + 1], dirty_rect_list[i]);
                    dirty_rect_list.splice(i + 1, 1);

                    if (i > 0) i--;
                } else {
                    i++;
                }
            }

            var dirty_rect_list__length = dirty_rect_list.length,
                dupes = {},
                entities_to_draw = [];

            ctx = ctx || MainLoop.getContext();

            // Debug variables, if we're debugging, ya-hear?
            var __debug_entities_drawn = 0,
                __debug_drawn_list = Config.canvas.debug_dirty ? [] : null,
                __debug_drawn_no = 0,
                __debug_ctx = null;

            for (i = 0; i < dirty_rect_list__length; i++) {
                __debug_drawn_no = 0;
                dirty_rect = dirty_rect_list[i];
                dupes = {};
                entities_to_draw.length = 0;
                if (!dirty_rect) continue;

                dirty_rect._w = dirty_rect._x + dirty_rect._w;
                dirty_rect._h = dirty_rect._y + dirty_rect._h;
                dirty_rect._x = (dirty_rect._x > 0) ? (dirty_rect._x | 0) : (dirty_rect._x | 0) - 1;
                dirty_rect._y = (dirty_rect._y > 0) ? (dirty_rect._y | 0) : (dirty_rect._y | 0) - 1;
                dirty_rect._w -= dirty_rect._x;
                dirty_rect._h -= dirty_rect._y;
                dirty_rect._w = (dirty_rect._w === (dirty_rect._w | 0)) ? dirty_rect._w : (dirty_rect._w | 0) + 1;
                dirty_rect._h = (dirty_rect._h === (dirty_rect._h | 0)) ? dirty_rect._h : (dirty_rect._h | 0) + 1;

                entity_list = HashMap.search(dirty_rect, false);

                ctx.clearRect(dirty_rect._x, dirty_rect._y, dirty_rect._w, dirty_rect._h);

                ctx.save();
                ctx.beginPath();
                ctx.rect(dirty_rect._x, dirty_rect._y, dirty_rect._w, dirty_rect._h);
                ctx.clip();

                for (j = 0, entity_list__length = entity_list.length; j < entity_list__length; j++) {
                    entity = entity_list[j];

                    if (!dupes[entity[0]] && entity._visible) {
                        entities_to_draw.push(entity);
                        dupes[entity[0]] = true;
                    }

                }

                if (!__debug_ctx && (Config.canvas.debug_mbr || Config.canvas.debug_entity_maps)) {
                    __debug_ctx = DebugCanvas.getDebugContext();
                    DebugCanvas.clearDebugCanvas();
                }

                entities_to_draw.sort(zsort);
                for (j = 0, entity_list__length = entities_to_draw.length; j < entity_list__length; j++) {
                    entity = entities_to_draw[j];
                    entity__mbr = entity._mbr || entity;

                    // rect_overlap, inlined.
                    if (dirty_rect._y < entity__mbr._y + entity__mbr._h
                        && dirty_rect._x < entity__mbr._x + entity__mbr._w
                        && entity__mbr._y < dirty_rect._y + dirty_rect._h
                        && entity__mbr._x < dirty_rect._x + dirty_rect._w) {

                        // draw the entity.
                        entity.draw(ctx);

                        // debug layer.
                        if (Config.canvas.debug_entity_maps && entity.map && entity.map.points)
                            drawDebugPolygon(__debug_ctx, entity);

                        if (Config.canvas.debug_mbr) {
                            if (!entity._mbr || Config.canvas.debug_both_rects) {
                                __debug_ctx.strokeStyle = entity._mbr ? 'gray' : 'green';
                                __debug_ctx.strokeRect(entity._x, entity._y, entity._w, entity._h);
                            }

                            if (entity._mbr) {
                                __debug_ctx.strokeStyle = 'red';
                                __debug_ctx.strokeRect(entity._mbr._x, entity._mbr._y, entity._mbr._w, entity._mbr._h);
                            }
                        }

                        __debug_drawn_no++;
                    }

                }

                __debug_entities_drawn += __debug_drawn_no;
                __debug_drawn_list && __debug_drawn_list.push(__debug_drawn_no);

                ctx.closePath();
                ctx.restore();
            }

            if (__debug_drawn_list) {
                if (!__debug_ctx) {
                    __debug_ctx = DebugCanvas.getDebugContext();
                    DebugCanvas.clearDebugCanvas();
                }

                __debug_ctx.save();
                __debug_ctx.globalAlpha = 1.0;
                __debug_ctx.font = "Bold 14px Courier New";
                __debug_ctx.fillStyle = "red";
                __debug_ctx.strokeStyle = 'blue';
                __debug_ctx.lineWidth = 3;

                for (i = 0; i < dirty_rect_list__length; ++i) {
                    dirty_rect = dirty_rect_list[i];
                    __debug_ctx.fillText(__debug_drawn_list[i] + " entities", dirty_rect._x, dirty_rect._y - 4);
                    __debug_ctx.strokeRect(dirty_rect._x - 3, dirty_rect._y - 3, dirty_rect._w + 6, dirty_rect._h + 6)
                }

                __debug_ctx.restore();
            }
            GlobalEvents.trigger('metrics.entities_drawn', __debug_entities_drawn);

            rect__clean();
        }
    };

});