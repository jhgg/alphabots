define(['Base/Entity', 'Canvas/DrawManager', 'Helpers/RectPool', 'Game/MainLoop'],
    function (Entity, DrawManager, RectPool, MainLoop) {

        var drawInfo = {
            type: "canvas",
            pos: {},
            ctx: null,
            coord: [0, 0, 0, 0],
            co: {
                x: 0,
                y: 0,
                w: 0,
                h: 0
            }
        };

        return Entity.extend({
            init: function (sprite) {
                this._super();
                this.sprite = sprite;
                this.currentRect = {};
                this._changed = true;
                this.w = sprite.width;
                this.h = sprite.height;

                DrawManager.addEntity(this);

                // When we change, we will alert the manager that we need to be
                // updated on the map!
                this.on("change", function () {
                    if (this._changed === false) {
                        this._changed = true;
                        DrawManager.addEntity(this);
                    }
                });

                // When we're destroyed, we'll clear our part on the map too!
                this.on("destroy", function () {
                    this._changed = true;
                    DrawManager.addEntity(this);
                });

                this.on('draw', function(e) {
                    e.ctx.drawImage(this.sprite, e.pos._x, e.pos._y);
                });
            },

            draw: function (context) {
                context = context || MainLoop.getContext();
                var coord = this.__coord || [0, 0, 0, 0],
                    oldAlpha,
                    pos = drawInfo.pos,
                    co = drawInfo.co,
                    context_saved = false;

                pos._x = this._x;
                pos._y = this._y;
                pos._w = this._w;
                pos._h = this._h;

                co.x = coord[0];
                co.y = coord[1];
                co.w = coord[2];
                co.h = coord[3];

                if (this._mbr) {
                    context.save();
                    context_saved = true;
                    context.translate(this._origin.x + this._x, this._origin.y + this._y);
                    //context.translate(this._x, this._y);
                    pos._x = -this._origin.x;
                    pos._y = -this._origin.y;
                    context.rotate((this._rotation % 360) * (Math.PI / 180));
                }

                if (this._alpha < 1.0) {
                    oldAlpha = context.globalAlpha;
                    context.globalAlpha = this._alpha;
                }

                drawInfo.ctx = context;
                this.trigger('draw', drawInfo);

                if (context_saved) {
                    context.restore();
                }

                if (oldAlpha !== undefined) {
                    context.globalAlpha = oldAlpha;
                }

                drawInfo.ctx = null;
                return this;
            }


        });
    });