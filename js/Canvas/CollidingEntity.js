define(['Canvas/Entity', 'Collision/Shape/Polygon', 'Collision/HashMap', 'Collision/Masks'],
function (Entity, Polygon, HashMap, CollisionMasks) {
    var DEG_TO_RAD = Math.PI / 180;

    return Entity.extend({
        collision_mask: CollisionMasks.DEFAULT,
        init: function (sprite) {
            this._super(sprite || this.sprite);
        },

        init_collision_with_poly: function (poly) {
            this.off('resize', this._resize_map);
            if (this.map)
                this.removeChild(this.map);

            if (!poly) {
                poly = new Polygon([0, 0], [this._w, 0], [this._w, this._h], [0, this._h]);
                this.on('resize', this._resize_map);
            }

            if (this.rotation)
                poly.rotate({
                    cos: Math.cos(-this.rotation * DEG_TO_RAD),
                    sin: Math.sin(-this.rotation * DEG_TO_RAD),
                    o: {
                        x: this._origin.x,
                        y: this._origin.y
                    }
                });

            if (arguments.length > 1) {
                var args = Array.prototype.slice.call(arguments, 0);
                poly = new Polygon(args);
            }

            this.map = poly;
            this.addChild(poly);
            poly.shift(this._x, this._y);
            return this;
        },

        _resize_map: function (e) {
            var dx, dy, rot = this.rotation * DEG_TO_RAD,
                points = this.map.points;

            if (e.axis === 'w') {

                if (rot) {
                    dx = e.amount * Math.cos(rot);
                    dy = e.amount * Math.sin(rot);
                } else {
                    dx = e.amount;
                    dy = 0;
                }

                points[1][0] += dx;
                points[1][1] += dy;
            } else {

                if (rot) {
                    dy = e.amount * Math.cos(rot);
                    dx = -e.amount * Math.sin(rot);
                } else {
                    dx = 0;
                    dy = e.amount;
                }

                points[3][0] += dx;
                points[3][1] += dy;
            }

            points[2][0] += dx;
            points[2][1] += dy;

        },
        hit: function (collision_mask) {
            var area = this._mbr || this,
                results = HashMap.search(area, false),
                i = 0,
                l = results.length,
                dupes = {},
                id, obj, oarea, key,
                hasMap = !!(this.map && this.map.containsPoint),
                finalresult = [];

            if (!l) {
                return false;
            }

            for (; i < l; ++i) {
                obj = results[i];
                oarea = obj._mbr || obj; //use the mbr

                if (!obj) continue;
                id = obj[0];

                //check if not added to hash and that actually intersects
                if (   !dupes[id]
                    && this[0] !== id
                    && obj.collision_mask
                    && (obj.collision_mask & collision_mask) !== 0
                    && oarea._x < area._x + area._w
                    && oarea._x + oarea._w > area._x
                    && oarea._y < area._y + area._h
                    && oarea._h + oarea._y > area._y)

                    dupes[id] = obj;
            }

            for (key in dupes) {
                obj = dupes[key];
                if (hasMap && obj.map) {
                    var SAT = this._SAT(this.map, obj.map);
                    if (!SAT)
                        continue;

                    SAT.obj = obj;
                    SAT.type = "SAT";
                    finalresult.push(SAT);
                } else {
                    finalresult.push({
                        obj: obj,
                        type: "MBR"
                    });
                }
            }

            if (!finalresult.length) {
                return false;
            }

            return finalresult;
        },

        onHit: function (collision_mask, callback, callbackOff) {
            var justHit = false;
            callbackOff = typeof callbackOff === 'function' ? callbackOff : false;
            this.on("update", function () {
                var hitdata = this.hit(collision_mask);
                if (hitdata) {
                    justHit = true;
                    callback.call(this, hitdata);
                } else if (justHit) {
                    callbackOff && callbackOff.call(this);
                    justHit = false;
                }
            });
            return this;
        },

        _SAT: function (poly1, poly2) {
            var points1 = poly1.points,
                points2 = poly2.points,
                i = 0,
                l = points1.length,
                j, k = points2.length,
                normal_x = 0,
                normal_y = 0,
                length,
                min1, min2,
                max1, max2,
                interval,
                MTV = null,
                MTV2 = null,
                MN_x,
                MN_y,
                dot,
                nextPoint,
                currentPoint,
                sqrt = Math.sqrt;

            //loop through the edges of Polygon 1
            for (; i < l; i++) {
                nextPoint = points1[(i == l - 1 ? 0 : i + 1)];
                currentPoint = points1[i];

                //generate the normal for the current edge
                normal_x = -(nextPoint[1] - currentPoint[1]);
                normal_y = (nextPoint[0] - currentPoint[0]);

                //normalize the vector
                length = sqrt(normal_x * normal_x + normal_y * normal_y);
                normal_x /= length;
                normal_y /= length;

                //project all vertices from poly1 onto axis
                min1 = max1 = points1[0][0] * normal_x + points1[0][1] * normal_y;
                for (j = 1; j < l; ++j) {
                    dot = points1[j][0] * normal_x + points1[j][1] * normal_y;
                    if (dot > max1) max1 = dot;
                    if (dot < min1) min1 = dot;
                }

                //project all vertices from poly2 onto axis
                min2 = max2 = points2[0][0] * normal_x + points2[0][1] * normal_y;
                for (j = 1; j < k; ++j) {
                    dot = points2[j][0] * normal_x + points2[j][1] * normal_y;
                    if (dot > max2) max2 = dot;
                    if (dot < min2) min2 = dot;
                }

                //calculate the minimum translation vector should be negative
                if (min1 < min2) {
                    interval = min2 - max1;

                    normal_x = -normal_x;
                    normal_y = -normal_y;
                } else {
                    interval = min1 - max2;
                }

                //exit early if positive
                if (interval >= 0) {
                    return false;
                }

                if (MTV === null || interval > MTV) {
                    MTV = interval;
                    MN_x = normal_x;
                    MN_y = normal_y;
                }
            }

            //loop through the edges of Polygon 2
            for (i = 0; i < k; i++) {
                nextPoint = points2[(i == k - 1 ? 0 : i + 1)];
                currentPoint = points2[i];

                //generate the normal for the current edge
                normal_x = -(nextPoint[1] - currentPoint[1]);
                normal_y = (nextPoint[0] - currentPoint[0]);

                //normalize the vector
                length = sqrt(normal_x * normal_x + normal_y * normal_y);
                normal_x /= length;
                normal_y /= length;

                //default min max
                min1 = min2 = -1;
                max1 = max2 = -1;

                //project all vertices from poly1 onto axis
                min1 = max1 = points1[0][0] * normal_x + points1[0][1] * normal_y;
                for (j = 1; j < l; ++j) {
                    dot = points1[j][0] * normal_x + points1[j][1] * normal_y;
                    if (dot > max1) max1 = dot;
                    if (dot < min1) min1 = dot;
                }

                //project all vertices from poly2 onto axis
                min2 = max2 = points2[0][0] * normal_x + points2[0][1] * normal_y;
                for (j = 1; j < k; ++j) {
                    dot = points2[j][0] * normal_x + points2[j][1] * normal_y;
                    if (dot > max2) max2 = dot;
                    if (dot < min2) min2 = dot;
                }

                //calculate the minimum translation vector should be negative
                if (min1 < min2) {
                    interval = min2 - max1;

                    normal_x = -normal_x;
                    normal_y = -normal_y;
                } else
                    interval = min1 - max2;

                //exit early if positive
                if (interval >= 0) {
                    return false;
                }

                if (MTV === null || interval > MTV) MTV = interval;
                if (interval > MTV2 || MTV2 === null) {
                    MTV2 = interval;
                    MN_x = normal_x;
                    MN_y = normal_y;
                }
            }

            return {
                overlap: MTV2,
                normal: {
                    x: MN_x,
                    y: MN_y
                }
            };
        }


    });
});