define(['lib/underscore', 'Game/GlobalEvents'], function(_, GlobalEvents) {
    var entities = {},
        num_entities = 0,
        handlers = {},
        GUID = 0;

    var exports = {};

    exports.addEntity = function (entity) {
        var guid = entity[0];
        if (entities[guid] === undefined) {
            entities[guid] = entity;
            num_entities++;
            GlobalEvents.trigger('metrics.num_entities', num_entities);
        }
    };

    exports.removeEntity = function (entity) {
        var guid = entity[0];
        if (entities[guid] !== undefined) {
            delete entities[guid];
            num_entities--;
            GlobalEvents.trigger('metrics.num_entities', num_entities);
        }
    };

    exports.getEntityByGuid = function (guid) {
        return entities[guid];
    };

    exports.generateGUID = function() {
        return ++GUID;
    };

    exports.trigger = function (event, data) {
        handlers[event] && _.forEach(handlers[event], function(handlers, guid) {
            var entity = entities[guid];
            if (!entity || !handlers) return;

            _.forEach(handlers, function(handler) {
                handler.call(entity, data);
            });
        });
    };

    Object.defineProperty(exports, 'num_entities', {
        get: function() {
            return num_entities;
        }
    });

    Object.defineProperty(exports, 'entities', {
        get: function() {
            return entities;
        }
    });

    Object.defineProperty(exports, 'handlers', {
        get: function() {
            return handlers;
        }
    });

    return exports;

});