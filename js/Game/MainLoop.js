define(['Game/EntityManager', 'Canvas/DrawManager', 'Game/GlobalEvents'],
    function (EntityManager, DrawManager, GlobalEvents) {
        var _canvas = null,
            _ctx = null,
            exports = {},
            isActive = false,
            curTime,
            el = document.getElementById("clock"),
            frames = 0,
            endTime = 0,
            FPS = 50,
            msPerFrame = (1000 / FPS) | 0,
            timeSlip = 0,
            draws = 0,
            gameTime;

        // Setters
        exports.setCanvas = function (canvas, ctx) {
            _canvas = canvas;
            _ctx = ctx || canvas.getContext('2d');

        };

        // Getters

        exports.getCanvas = function () {
            return _canvas;
        };
        exports.getContext = function () {
            return _ctx;
        };

        function tick() {
            isActive && requestAnimationFrame(tick);

            var curTime = Date.now(), loops, dt, lastFrameTime;

            if (endTime)
                GlobalEvents.trigger("metric.frame_wait", curTime - endTime);

            // We're ahead, wtf?
            if (gameTime + timeSlip >= curTime) {
                endTime = curTime;
                return;
            }

            // How much time changed
            var netTimeStep = curTime - (gameTime + timeSlip);
            if (netTimeStep > msPerFrame * 20) {
                timeSlip += netTimeStep - msPerFrame;
                netTimeStep = msPerFrame;
            }

            loops = Math.ceil(netTimeStep / msPerFrame);
            // maxFramesPerStep adjusts how willing we are to delay drawing in order to keep at the target FPS
            loops = Math.min(loops, 5);
            dt = msPerFrame;

            for (var i = 0; i < loops; i++) {
                lastFrameTime = curTime;
                $$game.gameTime = gameTime;
                // Everything that changes over time hooks into this event
                EntityManager.trigger("update", {
                    frame: frames++,
                    dt: dt,
                    gameTime: gameTime
                });
                gameTime += dt;
                curTime = Date.now();
                GlobalEvents.trigger("metrics.frame_time", curTime - lastFrameTime);
            }

            if (loops) {
                draws++;
                var drawTimeStart = curTime;
                DrawManager.draw(_ctx);
                EntityManager.trigger("draw_finished");
                curTime = Date.now();
                GlobalEvents.trigger("metrics.draw_time", curTime - drawTimeStart);
            }
        }

        var _last_frame_ctr, _last_call;
        setInterval(function() {
            if (!_last_frame_ctr) {
                _last_frame_ctr = draws;
                _last_call = Date.now();
                return;
            }
            var curTime = Date.now();
            var fps = ((draws - _last_frame_ctr) / (curTime - _last_call)) * 1000;
            _last_call = curTime;
            _last_frame_ctr = draws;
            GlobalEvents.trigger('metrics.dps', fps);
        }, 500);

        exports.start = function () {
            if (isActive)
                return;
            isActive = true;

            curTime = Date.now();
            gameTime = curTime - msPerFrame;
            requestAnimationFrame(tick);
        };

        exports.stop = function() {
            if (isActive)
                isActive = false;
        };

        return exports;

    });