define(function () {
    var handlers = {};

    return {
        // Register a callback to this event.
        on: function (event, callback) {
            if (!handlers[event]) handlers[event] = [];

            handlers[event].push(callback);
            return this;
        },

        // Register a callback to an event that only gets fired once.
        one: function (event, callback) {
            var self = this;
            self.on(event, function (data) {
                callback.call(self, data);
                this.off(event, callback);
            });
        },

        // Deregister a calblback from an event. If no callback is provided, removes all callbacks.
        off: function (event, callback) {
            var eventHandlers = handlers[event],
                eventHandlers_length;

            if (eventHandlers)
                eventHandlers_length = eventHandlers.length;
            else
                return this;

            if (!callback) {
                delete handlers[event];
                return this;
            }

            var num_deleted_handlers = 0;

            for (var i = 0; i < eventHandlers_length; i++) {
                if (eventHandlers[i] === callback) {
                    delete eventHandlers[i];
                    num_deleted_handlers++;
                }
            }

            if (num_deleted_handlers == eventHandlers_length) {
                delete handlers[event];
            }

            return this;
        },

        uniqueOn: function (event, callback) {
            this.off(event, callback);
            this.on(event, callback);
        },

        trigger: function (event, data) {
            var eventHandlers = handlers[event],
                eventHandlers__length,
                current;

            if (eventHandlers) {
                eventHandlers__length = eventHandlers.length;

                for (var i = 0; i < eventHandlers__length; i++) {
                    current = eventHandlers[i];
                    if (current === undefined) {
                        eventHandlers.splice(i, 1);
                        eventHandlers__length--;
                        i--;
                    } else {
                        current.call(this, data);
                    }
                }

                if (!eventHandlers__length)
                    delete handlers[event];

            }
            return this;
        }
    }
});

