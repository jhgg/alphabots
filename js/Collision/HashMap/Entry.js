define(['lib/class', 'Collision/HashMap/Util'], function (Class, Util) {
    return Class.extend({
        init: function (keys, obj, map) {
            this.keys = keys;
            this.map = map;
            this.obj = obj;
        },

        update: function(rect) {
            if (Util.hash(Util.key(rect)) != Util.hash(this.keys)) {
                this.map.refresh(this);
            }
        }
    })
});