define(['Config'], function (Config) {
    var exports = {},
        SPACE = " ",
        cellsize = Config.collision.hashmap.cellsize;

    exports.key = function (obj, keys) {
        if (obj._mbr) {
            obj = obj._mbr;
        }
        if (!keys) {
            keys = {};
        }

        keys.x1 = (obj._x / cellsize) | 0;
        keys.y1 = (obj._y / cellsize) | 0;
        keys.x2 = ((obj._w + obj._x) / cellsize) | 0;
        keys.y2 = ((obj._h + obj._y) / cellsize) | 0;
        return keys;
    };

    exports.hash = function (keys) {
        return keys.x1 + SPACE + keys.y1 + SPACE + keys.x2 + SPACE + keys.y2;
    };

    return exports;
});