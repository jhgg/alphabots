// HashMap, adapted from Crafty.js, by Lois Stowasser,
// Based upon Broad-Phase Collision.

define(['lib/class', 'Collision/HashMap/Entry', 'Collision/HashMap/Util'],
    function (Class, Entry, Util) {
        var HashMap = Class.extend({
            init: function () {
                this.map = {};
            },

            insert: function (obj) {
                var keys = Util.key(obj),
                    entry = new Entry(keys, obj, this),
                    i,
                    j,
                    hash;

                //insert into all x buckets
                for (i = keys.x1; i <= keys.x2; i++) {
                    //insert into all y buckets
                    for (j = keys.y1; j <= keys.y2; j++) {
                        hash = (i << 16) ^ j;
                        if (!this.map[hash]) this.map[hash] = [];
                        this.map[hash].push(obj);
                    }
                }

                return entry;
            },

            search: function (rect, filter) {
                var keys = Util.key(rect),
                    i, j, k, l,
                    results = [],
                    cell, cell_len,
                    keys_x1 = keys.x1, keys_x2 = keys.x2,
                    keys_y1 = keys.y1, keys_y2 = keys.y2,
                    map = this.map;

                for (i = keys_x1; i <= keys_x2; i++) {
                    for (j = keys_y1; j <= keys_y2; j++) {
                        cell = map[(i << 16) ^ j];
                        if (!cell)
                            continue;

                        for (k = 0, cell_len = cell.length; k < cell_len; k++)
                            results.push(cell[k]);
                    }
                }

                if (filter !== undefined && !filter)
                    return results;

                var obj, id, finalresult = [],
                    found = {};
                //add unique elements to lookup table with the entity ID as unique key
                for (i = 0, l = results.length; i < l; i++) {
                    obj = results[i];
                    if (!obj) continue; //skip if deleted
                    id = obj[0]; //unique ID
                    obj = obj._mbr || obj;
                    //check if not added to hash and that actually intersects
                    if (!found[id] && obj._x < rect._x + rect._w && obj._x + obj._w > rect._x &&
                        obj._y < rect._y + rect._h && obj._h + obj._y > rect._y) {
                        found[id] = true;
                        finalresult.push(results[i]);
                    }
                }

                return finalresult;
            },

            remove: function (keys, obj) {
                var i,
                    j, hash;

                if (arguments.length == 1) {
                    obj = keys;
                    keys = Util.key(obj);
                }

                //search in all x buckets
                for (i = keys.x1; i <= keys.x2; i++) {
                    //insert into all y buckets
                    for (j = keys.y1; j <= keys.y2; j++) {
                        hash = (i << 16) ^ j;

                        if (this.map[hash]) {
                            var cell = this.map[hash],
                                m, n = cell.length;
                            //loop over objs in cell and delete
                            for (m = 0; m < n; m++)
                                if (cell[m] && cell[m][0] === obj[0])
                                    cell.splice(m, 1);
                        }
                    }
                }
            },

            refresh: function (entry) {
                var keys = entry.keys;
                var obj = entry.obj;
                var cell, i, j, m, n;

                //First delete current object from appropriate cells
                var keys_x1 = keys.x1;
                var keys_x2 = keys.x2;
                var keys_y1 = keys.y1;
                var keys_y2 = keys.y2;
                var map = this.map;
                var guid = obj[0];

                for (i = keys_x1; i <= keys_x2; i++) {
                    for (j = keys_y1; j <= keys_y2; j++) {
                        cell = map[(i << 16) ^ j];
                        if (cell) {
                            //loop over objs in cell and delete
                            for (m = 0, n = cell.length; m < n; m++)
                                if (cell[m] && cell[m][0] === guid)
                                    cell.splice(m, 1);
                        }
                    }
                }

                Util.key(obj, keys);
                keys_x1 = keys.x1;
                keys_x2 = keys.x2;
                keys_y1 = keys.y1;
                keys_y2 = keys.y2;

                //insert into all rows and columns
                for (i = keys_x1; i <= keys_x2; i++) {
                    for (j = keys_y1; j <= keys_y2; j++) {
                        cell = map[(i << 16) ^ j];
                        if (!cell) cell = map[(i << 16) ^ j] = [];
                        cell.push(obj);
                    }
                }

                return entry;
            },

            boundaries: function () {
                var k, ent,
                    hash = {
                        max: {
                            x: -Infinity,
                            y: -Infinity
                        },
                        min: {
                            x: Infinity,
                            y: Infinity
                        }
                    },
                    coords = {
                        max: {
                            x: -Infinity,
                            y: -Infinity
                        },
                        min: {
                            x: Infinity,
                            y: Infinity
                        }
                    };

                //Using broad phase hash to speed up the computation of boundaries.
                for (var h in this.map) {
                    if (!this.map[h].length) continue;

                    //broad phase coordinate
                    var i = h >> 16,
                        j = (h << 16) >> 16;
                    if (j < 0) {
                        i = i ^ -1;
                    }
                    if (i >= hash.max.x) {
                        hash.max.x = i;
                        for (k in this.map[h]) {
                            ent = this.map[h][k];
                            if (typeof ent == 'object' && ent._is_object) {
                                coords.max.x = Math.max(coords.max.x, ent.x + ent.w);
                            }
                        }
                    }
                    if (i <= hash.min.x) {
                        hash.min.x = i;
                        for (k in this.map[h]) {
                            ent = this.map[h][k];
                            if (typeof ent == 'object' && ent._is_object) {
                                coords.min.x = Math.min(coords.min.x, ent.x);
                            }
                        }
                    }
                    if (j >= hash.max.y) {
                        hash.max.y = j;
                        for (k in this.map[h]) {
                            ent = this.map[h][k];
                            if (typeof ent == 'object' && ent._is_object) {
                                coords.max.y = Math.max(coords.max.y, ent.y + ent.h);
                            }
                        }
                    }
                    if (j <= hash.min.y) {
                        hash.min.y = j;
                        for (k in this.map[h]) {
                            ent = this.map[h][k];
                            if (typeof ent == 'object' && ent._is_object) {
                                coords.min.y = Math.min(coords.min.y, ent.y);
                            }
                        }
                    }
                }

                return coords;
            }

        });

        return new HashMap();

    });