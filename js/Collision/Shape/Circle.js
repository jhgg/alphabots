define(['lib/class'], function (Class) {
    return Class.extend({
        init: function (x, y, radius) {
            this.x = x;
            this.y = y;
            this.radius = radius;
        },

        containsPoint: function (x, y) {
            var radius = this.radius,
                dX = this.x - x,
                dY = this.y - y;

            return (dX * dX + dY * dY) < (radius * radius);
        },

        shift: function(x, y) {
            this.x += x;
            this.y += y;
        },

        rotate: function() {}

    });

});