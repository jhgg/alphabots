define(['lib/class'], function (Class) {
    var Polygon =  Class.extend({
        init: function (points) {
            if (arguments.length > 1)
                points = Array.prototype.slice.call(arguments, 0);

            this.points = points;
        },
        containsPoint: function (x, y) {
            var points = this.points,
                i, j, contains = false,
                points_length = points.length;

            for (i = 0, j = points_length - 1; i < points_length; j = i++)
                if (((points[i][1] > y) != (points[j][1] > y)) &&
                    (x < (points[j][0] - points[i][0]) * (y - points[i][1]) / (points[j][1] - points[i][1]) + points[i][0]))
                    contains = !contains;

            return contains;

        },
        shift: function (x, y) {
            var i = 0,
                points_length = this.points.length,
                current,
                points = this.points;

            for (; i < points_length; i++) {
                current = points[i];
                current[0] += x;
                current[1] += y;
            }

        },
        clone: function() {
            var points = [],
                i, l;

            for(i = 0, l = this.points.length; i < l; i++)
                points.push([this.points[i][0], this.points[i][1]]);

            return new Polygon(points);

        },
        rotate: function (e) {
            var i = 0,
                points_length = this.points.length,
                points = this.points,
                current, x, y,
                x2 = e.o.x,
                y2 = e.o.y,
                sin = e.sin,
                cos = e.cos;

            for (; i < points_length; i++) {
                current = points[i];
                x = x2 + (current[0] - x2) * cos + (current[1] - y2) * sin;
                y = y2 - (current[0] - x2) * sin + (current[1] - y2) * cos;

                current[0] = x;
                current[1] = y;
            }
        }
    });

    return Polygon;
});