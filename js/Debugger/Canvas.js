define(['Config'], function(Config) {
    var exports = {},
        _debugCanvas = null,
        _debugCanvasContext = null;

    exports.getDebugCanvas = function() {
        if (!_debugCanvas) {
            _debugCanvas = document.createElement("canvas");
            _debugCanvas.width  = Config.canvas.width;
            _debugCanvas.height = Config.canvas.height;
            _debugCanvasContext = _debugCanvas.getContext('2d');
            _debugCanvasContext.globalAlpha = 0.5;
            var stage = window.$$game.getGameStage();
            stage.appendChild(_debugCanvas);
        }
        return _debugCanvas;
    };

    exports.clearDebugCanvas = function() {
        if (!_debugCanvas)
            return;

        _debugCanvasContext.clearRect(0, 0, _debugCanvas.width, _debugCanvas.height);
    };

    exports.getDebugContext = function() {
        if (!_debugCanvas) {
            exports.getDebugCanvas();
        }

        return _debugCanvasContext;
    };

    return exports;

});