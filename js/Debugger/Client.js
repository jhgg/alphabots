define(['Game/GlobalEvents'], function(GlobalEvents) {
    var triggers_bound = false,
        debug_window;

    function make_listener(out_series) {
        return function(data) {
            debug_window && debug_window.postMessage(
                {
                    series: out_series,
                    value: data,
                    time: Date.now()
                },
                document.location.origin
            );
        }
    }

    function bind_triggers() {
        GlobalEvents.on('metrics.dps', make_listener('dps'));
        GlobalEvents.on('metrics.entities_drawn', make_listener('entities_drawn'));
        GlobalEvents.on('metrics.draw_time', make_listener('draw_time'));
        GlobalEvents.on('metrics.frame_time', make_listener('frame_time'));

        window.addEventListener('beforeunload', function() {
            debug_window && debug_window.close();
        }, false);
    }

    function onLoadDebugger() {
        if (!triggers_bound)
            bind_triggers();

        if (!debug_window || debug_window.closed)
            debug_window = window.open('debugger.html', '', 'width=420,height=820');

    }

    return {
        bindDebugStartButton: function(selector) {
            var el = document.querySelector(selector);
            if (!el) return;

            el.addEventListener('click', onLoadDebugger, true);
        }

    }

});