from collections import defaultdict
import re

alnum = re.compile("^[a-z]+$")

fp = open("/usr/share/dict/words")
words = defaultdict(list)

for line in fp:

    word = line.strip().lower()
    sorted_word = ''.join(sorted(list(word)))
    if alnum.match(sorted_word) and len(sorted_word) <= 8:
        words[sorted_word].append(word)

fp = open("/js/Alphabots/Scrabble/WordList.json", "w")
import json
json.dump(words, fp, indent=4)