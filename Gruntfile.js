module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    'css/styles.css': 'scss/styles.scss'
                }
            }
        },

        watch: {
            grunt: { files: ['Gruntfile.js'] },
            css: {
                files: [
                    'css/**/*.css',
                    'css/views/**/*.css'
                ],
                options: {
                    livereload: true
                }
            },

            sass: {
                files: [
                    'scss/**/*.scss'
                ],
                tasks: ['sass']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['sass']);
    grunt.registerTask('default', ['build','watch']);
}
